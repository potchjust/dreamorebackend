<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>
        DreammoreApp- Login
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

</head>



<body class="auth-wrapper">
<div class="all-wrapper menu-side with-pattern">
    @if(\Illuminate\Support\Facades\Session::has('login_error'))
        <div class="alert alert-danger">{{\Illuminate\Support\Facades\Session::get('login_error')}}</div>
    @endif
    <div class="auth-box-w">
        <div class="logo-w">
            <a href="index-2.html">
                {{--TODO mettre le logo envoyé par Bruno--}}
            </a></div>
        <h4 class="auth-header">Formulaire de connexion</h4>
        <form action="{{route('login')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="username">Nom d'utilisateur</label>
                <input class="form-control" id="username" name="username" placeholder="Entrer votre nom d'utilisateur"
                       type="text">
                <div class="pre-icon os-icon os-icon-user-male-circle"></div>
            </div>
            <div class="form-group">
                <label for="password">Mot de passe</label>
                <input class="form-control" name="password" id="password" placeholder="Entrer votre mot de passe"
                       type="password">
                <div class="pre-icon os-icon os-icon-fingerprint"></div>
            </div>
            <div class="buttons-w">
                <button class="btn btn-primary">Se connecter</button>

            </div>
        </form>
    </div>
</div>

<!--   Core JS Files   -->
<script src="{{asset("js/jquery.min.js")}}" type="text/javascript"></script>
<script src="{{asset("js/popper.min.js")}}" type="text/javascript"></script>
</body>

</html>
