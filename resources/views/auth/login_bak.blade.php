<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>
        DreammoreApp- Login
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/material-kit.css')}}">

</head>

<body class="login-page ">
<div class="page-header header-filter"
     style="background-image: url(../img/bg7.jpg); background-size: cover; background-position: top center;">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 ml-auto mr-auto">
                <div class="card card-signup">
                    <form class="form" method="POST" action="{{route('login')}}">
                        @csrf
                        <div class="card-header card-header-primary text-center">
                            <h4 class="card-title">Dreammore</h4>
                        </div>
                        <p class="description text-center">Espace administration</p>
                        <div class="card-body">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">face</i>
                                        </span>
                                </div>
                                <input type="text" class="form-control" name="username"
                                       placeholder="Nom d'utilisateur ...">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">lock_outline</i>
                                        </span>
                                </div>
                                <input type="password" class="form-control" name="password" placeholder="Mot de passe">
                            </div>
                        </div>
                        <div class="footer text-center my-3">
                            <input type="submit" class="btn btn-outline-info btn-round" value="Se connecter"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!--   Core JS Files   -->
<script src="{{asset('js/core/jquery.min.js')}}"></script>
<script src="{{asset('js/core/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap-material-design.min.js')}}"></script>
<script src="{{asset('js/material-kit.min.js')}}"></script>
</body>

</html>
