<!doctype html>
<html lang="fr">
<head>
    <title>DreamoreApp- Dashboard</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <!--  Fonts and icons  -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="{{asset('css/app.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/_custom.css')}}" type="text/css"/>



</head>
<body class="menu-position-side menu-side-left full-screen with-content-panel">
<div class="all-wrapper with-side-panel solid-bg-all" id="app">

    <div class="layout-w">
        <!--------------------
START - Mobile Menu
-------------------->
        <div class="menu-mobile menu-activated-on-click color-scheme-dark">
            <div class="mm-logo-buttons-w">
                <router-link class="mm-logo" to="index-2.html"><img
                        src="img/logo.png"><span>DreamMore</span>
                </router-link>
                <div class="mm-buttons">
                    <div class="content-panel-open">
                        <div class="os-icon os-icon-grid-circles"></div>
                    </div>
                    <div class="mobile-menu-trigger">
                        <div class="os-icon os-icon-hamburger-menu-1"></div>
                    </div>
                </div>
            </div>
            <div class="menu-and-user">
                <div class="logged-user-w">
                    <div class="avatar-w">
                        <img alt="" src="{{asset('img/avatar_admin.png')}}"/>
                    </div>
                    <div class="logged-user-info-w">
                        <div class="logged-user-name">{{Auth::user()->username}}</div>
                    </div>
                </div>

                @include('layouts.includes._mobile_nav')

            </div>
        </div>

    @include('layouts.includes.nav')
    <!--------------------
END - Main Menu
-------------------->
        <div class="content-w">
            <!--------------------
START - Top Bar
-------------------->
            <div class="top-bar color-scheme-transparent">
                <!--------------------
START - Top Menu Controls
-------------------->
                <div class="top-menu-controls">

                    <!--------------------
START - Messages Link in secondary top menu
-------------------->
                    <div class="messages-notifications os-dropdown-trigger os-dropdown-position-left"><i
                            class="os-icon os-icon-mail-14"></i>
                        <div class="new-messages-count">12</div>
                        <div class="os-dropdown light message-list">
                            <ul>
                                <li><a href="#">
                                        <div class="user-avatar-w"><img alt="" src="img/avatar1.jpg"></div>
                                        <div class="message-content">
                                            <h6 class="message-from">John Mayers</h6>
                                            <h6 class="message-title">Account Update</h6>
                                        </div>
                                    </a></li>
                                <li><a href="#">
                                        <div class="user-avatar-w"><img alt="" src="img/avatar2.jpg"></div>
                                        <div class="message-content">
                                            <h6 class="message-from">Phil Jones</h6>
                                            <h6 class="message-title">Secutiry Updates</h6>
                                        </div>
                                    </a></li>
                                <li><a href="#">
                                        <div class="user-avatar-w"><img alt="" src="img/avatar3.jpg"></div>
                                        <div class="message-content">
                                            <h6 class="message-from">Bekky Simpson</h6>
                                            <h6 class="message-title">Vacation Rentals</h6>
                                        </div>
                                    </a></li>
                                <li><a href="#">
                                        <div class="user-avatar-w"><img alt="" src="img/avatar4.jpg"></div>
                                        <div class="message-content">
                                            <h6 class="message-from">Alice Priskon</h6>
                                            <h6 class="message-title">Payment Confirmation</h6>
                                        </div>
                                    </a></li>
                            </ul>
                        </div>
                    </div>
                    <!--------------------
END - Messages Link in secondary top menu
-------------------->
                    <!--------------------
START - Settings Link in secondary top menu
-------------------->


                    <div class="logged-user-w">
                        <div class="logged-user-i">
                            <div class="avatar-w">
                                <img alt="" src="{{asset('img/avatar_admin.png')}}">
                            </div>
                            <div class="logged-user-menu color-style-bright">
                                <div class="logged-user-avatar-info">
                                    <div class="avatar-w"><img alt="" src="{{asset('img/avatar_admin.png')}}"></div>
                                    <div class="logged-user-info-w">
                                        <div
                                            class="logged-user-name">{{\Illuminate\Support\Facades\Auth::user()->username}}</div>
                                        <div class="logged-user-role">Administrator</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="content-panel-toggler"><i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
            </div>

            <div class="content-i">
                <router-view :auth_user="{{\Illuminate\Support\Facades\Auth::id()}}"></router-view>
            </div>
        </div>
    </div>
    <div class="display-type"></div>
</div>
<script src="{{mix('js/app.js')}}" type="text/javascript"></script>

<script src="{{asset("js/jquery.min.js")}}" type="text/javascript"></script>
<script src="{{asset("js/popper.min.js")}}" type="text/javascript"></script>

<script src="{{asset("js/bootstrap.min.js")}}" type="text/javascript"></script>
<script src="{{asset("js/perfect-scrollbar.jquery.min.js")}}"></script>
<script src="{{asset("js/bootstrap-notify.js")}}"></script>
</body>
</html>

