
<div

    class="menu-w color-scheme-light color-style-transparent menu-position-side menu-side-left menu-layout-compact sub-menu-style-over sub-menu-color-bright selected-menu-color-light menu-activated-on-hover menu-has-selected-link">
    <div class="logo-w"><a class="logo" href="index-2.html">
            <div class="logo-element"></div>
            <div class="logo-label">DreammoreApp Admin</div>
        </a></div>
    <div class="logged-user-w avatar-inline">
        <div class="logged-user-i">
            <div class="avatar-w">
                <img alt="" src="{{asset('img/avatar_admin.png')}}">
            </div>
            <div class="logged-user-info-w">
                <div class="logged-user-name">{{\Illuminate\Support\Facades\Auth::user()->username}}</div>
                <div class="logged-user-role">Administrator</div>
            </div>

        </div>
    </div>
    <div class="menu-actions">
        <!--------------------
START - Notifications new request Link in secondary top menu
-------------------->
            <a href="{{route('logout')}}">
                <i class="os-icon os-icon-power"></i>
            </a>

        <!--------------------
END - request Link in secondary top menu
-------------------->
    </div>
    <h1 class="menu-page-header">Page Header</h1>

    @can('is-admin')
    <ul class="main-menu">
        <li class="sub-header"><span>Fonctionnalités de base</span></li>
        <li class="selected has-sub-menu">
            <router-link to="/home">
                <div class="icon-w">
                    <div class="os-icon os-icon-layout"></div>
                </div>
                <span>Acceuil</span>
            </router-link>
        </li>
        {{--deuxieme partie--}}
        <li class="sub-header"><span>Partie requêtes</span></li>
        <li class="">
            <router-link :to="{name:'RequestHome'}">
                <div class="icon-w">
                    <div class="os-icon os-icon-package"></div>
                </div>
                <span>Requêtes</span>
            </router-link>
        </li>
        <li class="">
            <router-link :to="{name:'AssignRequestView'}">
                <div class="icon-w">
                    <div class="os-icon os-icon-package"></div>
                </div>
                <span>Assignation de requetes</span>
            </router-link>
        </li>
        <li class="">
            <router-link :to="{name:'requestTreatmentList'}">
                <div class="icon-w">
                    <div class="os-icon os-icon-git-pull-request"></div>
                </div>
                <span>Requetes en traitement</span>
            </router-link>
        </li>
        <li>
            <router-link :to="{name:'InQueueRequest'}">
                <div class="icon-w">
                    <div class="os-icon os-icon-file-text"></div>
                </div>
                <span>Requêtes en validation</span>
            </router-link>
        </li>
        <li class="">
            <router-link :to="{name:'RequestValidate'}">
                <div class="icon-w">
                    <div class="os-icon os-icon-checkmark"></div>
                </div>
                <span>Requetes validées</span>
            </router-link>
            <router-link :to="{name:'RequestCancel'}">
                <div class="icon-w">
                    <div class="os-icon os-icon-close"></div>
                </div>
                <span>Requetes Annulées</span>
            </router-link>
        </li>
        {{--Autres fonctionnalités--}}
        <li class="sub-header"><span>Partie commandes </span></li>
        <li class="">
            <router-link :to="{name:'preOrder'}">
                <div class="icon-w">
                    <div class="os-icon os-icon-mail"></div>
                </div>
                <span>Commandes </span>
            </router-link>
        </li>
     {{--   <li class="has-sub-menu">
            <router-link to="">
                <div class="icon-w">
                    <div class="os-icon os-icon-users"></div>
                </div>
                <span>Commandes</span>
            </router-link>
            <div class="sub-menu-w">
                <div class="sub-menu-i">
                    <ul class="sub-menu">
                        <li>
                            <router-link to="#">
                                <span>Commandes en cours </span>
                            </router-link>
                        </li>
                        <li>
                            <router-link to="#">
                                <span>Commandes annulées  </span>
                            </router-link>
                        </li>
                    </ul>
                </div>
            </div>

        </li>
        <li class="sub-header"><span> Autres </span></li>
        <li class=""><a href="#">
                <div class="icon-w">
                    <div class="os-icon os-icon-edit-32"></div>
                </div>
                <span>Rôles</span>
            </a>
        </li>--}}
        <li class="">
            <router-link :to="{name:'chatComponent'}">
                <div class="icon-w">
                    <div class="os-icon os-icon-edit-32"></div>
                </div>
                <span>Discussions </span>
            </router-link>
        </li>
    </ul>
    @endcan

    <ul class="main-menu">
        @can("is-request-manager")
        <li class="sub-header"><span>Fonctionnalités de base</span></li>
        <li class="selected has-sub-menu">
            <router-link :to="{name:'_managerHome'}">
                <div class="icon-w">
                    <div class="os-icon os-icon-layout"></div>
                </div>
                <span>Acceuil</span>
            </router-link>
        </li>
        {{--deuxieme partie--}}
        <li class="sub-header"><span>Partie requêtes</span></li>
        <li class="">
            <router-link :to="{name:'RequestHome'}">
                <div class="icon-w">
                    <div class="os-icon os-icon-package"></div>
                </div>
                <span>Requêtes reçues</span>
            </router-link>
        </li>
        <li class="">
            <router-link :to="{name:'_managerAssignedRequest'}">
                <div class="icon-w">
                    <div class="os-icon os-icon-package"></div>
                </div>
                <span>Mes Requêtes</span>
            </router-link>
        </li>

        <li class="">
            <router-link :to="{name:'RequestValidate'}">
                <div class="icon-w">
                    <div class="os-icon os-icon-checkmark"></div>
                </div>
                <span>Requetes validées</span>
            </router-link>
            <router-link :to="{name:'RequestCancel'}">
                <div class="icon-w">
                    <div class="os-icon os-icon-close"></div>
                </div>
                <span>Requetes Annulées</span>
            </router-link>
        </li>
        {{--Autres fonctionnalités--}}
        <li class="sub-header"><span>Partie commandes </span></li>
        <li class="">
            <router-link :to="{name:'preOrder'}">
                <div class="icon-w">
                    <div class="os-icon os-icon-mail"></div>
                </div>
                <span>Mes commandes </span>
            </router-link>
        </li>
        <li class="">
            <router-link :to="{name:'chatComponent'}">
                <div class="icon-w">
                    <div class="os-icon os-icon-edit-32"></div>
                </div>
                <span>Mes discussions </span>
            </router-link>
        </li>
        <li class="sub-header"><span>Autres</span></li>
        <li class="">
            <router-link :to="{name:'_notification'}">
                <div class="icon-w">
                    <div class="os-icon os-icon-bell"></div>
                </div>
                <span>Notifications</span>
            </router-link>
        </li>
        @endcan
            <li class="sub-header"><span>Autres</span></li>
            <li class="">
                <router-link :to="{name:'_fournisseur'}">
                    <div class="icon-w">
                        <div class="os-icon os-icon-bell"></div>
                    </div>
                    <span>Fournisseurs</span>
                </router-link>
            </li>
            <li class="">
                <router-link :to="{name:'_slidePage'}">
                    <div class="icon-w">
                        <div class="os-icon os-icon-image"></div>
                    </div>
                    <span>Slides</span>
                </router-link>
            </li>
    </ul>
    {{--fin navigation--}}


</div>

