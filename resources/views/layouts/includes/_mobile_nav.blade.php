<ul class="main-menu">
    <li class="sub-header"><span>Fonctionnalités de base</span></li>
    <li class="selected has-sub-menu">
        <router-link to="/home">
            <div class="icon-w">
                <div class="os-icon os-icon-layout"></div>
            </div>
            <span>Acceuil</span>
        </router-link>
    </li>
    {{--deuxieme partie--}}
    <li class="sub-header"><span>Partie requêtes</span></li>
    <li class="">
        <router-link to="/requetes">
            <div class="icon-w">
                <div class="os-icon os-icon-package"></div>
            </div>
            <span>Requêtes</span>
        </router-link>
    </li>
    <li><a href="#">
            <div class="icon-w">
                <div class="os-icon os-icon-file-text"></div>
            </div>
            <span>Bon de commandes</span>
        </a>
    </li>
    <li class=""><a href="#">
            <div class="icon-w">
                <div class="os-icon os-icon-life-buoy"></div>
            </div>
            <span>Commandes</span>
        </a>
    </li>
    {{--Autres fonctionnalités--}}
    <li class="sub-header"><span>Autres</span></li>
    <li class="">
        <a href="#">
            <div class="icon-w">
                <div class="os-icon os-icon-mail"></div>
            </div>
            <span>Emails</span>
        </a>
    </li>
    <li>
        <a href="#">
            <div class="icon-w">
                <div class="os-icon os-icon-users"></div>
            </div>
            <span>Utilisateurs</span>
        </a>
    </li>
    <li class=""><a href="#">
            <div class="icon-w">
                <div class="os-icon os-icon-edit-32"></div>
            </div>
            <span>Rôles</span>
        </a>
    </li>
</ul>
