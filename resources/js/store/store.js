import Vue from 'vue'
import Vuex from 'vuex'
import RequestStore from "./modules/RequestStore";
import AdminRequestStore from "./modules/admin/requestStore";
import SingleRequestStore from "./modules/SingleRequestStore";
import DiscussionStore from "./modules/DiscussionsStore"
import OrderStore from "./modules/OrderStore";
import MyRequestStore from "./modules/manager/MyRequestStore";
/*single*/


Vue.use(Vuex)
/**
 * Store des dernières requêtes
 */
export default new Vuex.Store({
    strict: true,
    modules: {
        RequestStore,
        SingleRequestStore,
        DiscussionStore,
        OrderStore,
        MyRequestStore,
        AdminRequestStore
    },
    state: {
        requetes: {},
        totalRequetes: 0,
        totalCommande: 0,
        totalBonCommande: 0,
        requete: {},
        auth_user:0,
        adminsList:[]
    },
    getters: {
        requetes: function (state) {
            return state.requetes
        },
        totalrequetes: function (state) {
            return state.totalRequetes
        },
        requete: function (state) {
            return state.requete
        },
        getAdmins:function (state) {
            return state.adminsList
        },
        getAuthId:function (state) {
            return state.authId
        }
    },
    mutations: {
        loadLast: function (state, {requetes}) {
            let requete = {}
            requetes.forEach(function (requet) {
                requete[requet.id] = requet
                state.requetes = requete
            });

        },
        loadSelectedRequest: function (state, {requete,}) {
            state.requete = requete
        },
        setAdminList:function (state,{adminsList}) {
            state.adminsList=adminsList;
        },
        setAuthId:function (state,{authId}) {
         state.authId=authId;
        },
        setLength:function (state, {request}) {
            state.totalRequetes=request.length
        }
    },
    actions: {
        /**
         * function devrant permettre de charger les dernièreqs requêtes
         */
        loadRequest: function (context) {
            axios.get('/lastFiveRequest').then(response => {
                //on ajoute les données au state
                context.commit('loadLast', {requetes: response.data.drafts})
            })
        },
        loadRequestContent: async function (context, requestId) {
            let response = await axios.get('/draft/details/' + 27);
            context.commit('loadSelectedRequest', {requete: response.data.draft, idRequest: requestId})
        },
        loadAdmins:async function(context){
            let  response=await axios.get('/request/validate/admin')
            context.commit('setAdminList',{adminsList:response.data})
        },
        loadAuthId:async function(context){
            let response=await axios.get('/admin/user');
            context.commit('setAuthId',{authId:response.data})
        },
        getAllRequestCount:async function(context){
            let response=await axios.get('/requestList');
            context.commit('setLength',{request:response.data.drafts})
        }
    }
})
