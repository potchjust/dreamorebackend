import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex);

//creer le store

const state = {
    requete: {},
    attachment: {},
    payment:{},
    file_count: 0,
    id_auth:0,
};
const getters = {
    getRequete: function (state) {
        return state.requete
    },
    getAttachment: function (state) {
        return state.attachment
    },
    getFileCount: function (state) {
        return state.file_count
    },
    getIdAuth:function(state){
        return state.id_auth;
    },
    is_request: function (state) {
        return state._is_request
    },
    getRequestPaymentProcess: function (state) {
        return state.payment
    }
};
const mutations = {

    LoadRequestContent: function (state, {content, attachment,payments, id}) {
        let file = {}
        let payment= {}
        state.requete = content
        attachment.forEach(function (item) {
            file[item.id] = item
            state.attachment = file
        })
        payments.forEach(function (_payment) {
            payment[_payment.id] = _payment
            state.payment = payments
        })

        state.file_count = attachment.length
    },

    setAuthId:function(state,{authId}){
        state.id_auth=authId
    }
};
const actions = {

    loadRequestComponent: async function (context, id_request) {
        let response = await axios.get('/draft/details/' + id_request);
        context.commit('LoadRequestContent', {
            content: response.data.draft,
            attachment: response.data.attachments,
            payments:response.data.payments,
            id: id_request
        })
    }
    ,
    /**
     * Pour marquer la requete comme en cours de traitement
     */
    markAsTreat: async function (context, {request_id, auth_id}) {

        let response = await axios.patch('/draft/' + request_id + '/attach', {
            request_id: request_id,
            user_id: auth_id
        });
    },
    getIdAuth:async function(context){
        let response=await axios.get('/admin/user');
        context.commit('setAuthId',{authId:response.data})
    },
    validateRequest:  async function (context, {formdata,request_id,config}) {
       return await axios.post('/requete/'+request_id,formdata,config);
    }

};

export default {
    state,
    mutations,
    getters,
    actions
}
