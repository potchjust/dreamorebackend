/*
store permettant de gérer la page requete}
 */
import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex);

const state = {
    //devra contenir toute la requêtes
    notreatList:{},
    requetes: {},
    vRequest:{},
    total_requete: 0,
    auth_id: 0,
    inQueueRequest: {},
    file: {},
    validateDetails:{},
    cancelList:{},
    cancelListCount:0,
    traitorsList:{},
    commandStatus:0,
};
/*pour retourner le objet dont on a besoin*/
const getters = {
    getRequetes: function (state) {
        return state.requetes
    },
    /**
     * recupérer la taille du tableau
     */
    getTotal: function (state) {
        return state.total_requete
    },

    getId: function (state) {
        return state.auth_id
    },
    getCancelCount:function(state){
        return state.cancelListCount
    },
    /**
     *get inQueue request
     */
    getInQueueRequest: function (state) {
        return state.inQueueRequest
    },
    getFile: function (state) {
        return state.file
    },
    getvRequest(state){
        return state.vRequest
    },
    getvalidateDetails(state){
        return state.validateDetails
    },
    getCancelList(state){
        return state.cancelList
    },
    getNotreatList(state){
        return state.notreatList;
    },
    getTraitorList(state){
        return state.traitorsList;
    },
    getCommandStatut(state){
        return state.commandStatus;
    }
};
/**
 * permet d'agir sur le state
 */
const mutations = {
    loadRequests: function (state, {drafts}) {
        let __requete = {};
        drafts.forEach(function (draft) {
            __requete[draft.id] = draft;
            state.requetes = __requete
        });
        state.total_requete = drafts.length
    },
    setAuthId: function (state, {auth_id}) {
        state.auth_id = auth_id;
    },
    /**
     * set the request
     */
    setInQueueRequest: function (state, {requests}) {
        let _queue = {};
        requests.map(function (queue) {
            _queue[queue.id] = queue
            state.inQueueRequest = _queue
        })

    },
    setAttachedFile: function (state, {file}) {
        state.file = file
    },
    setValidateRequest:function (state,{vRequest}) {
            let _request={};
        vRequest.map(function (request) {
                _request[request.id]=request
                state.vRequest=_request
            })
    },
    setValidateDetails:function (state,{validateDetails}) {
        state.validateDetails=validateDetails
    },
    setCancelList:function (state,{cancelList}) {
        let _cancelRequest={}
        cancelList.map(function (item) {
            _cancelRequest[item.id]=item
            state.cancelList=_cancelRequest
        })
        state.cancelListCount=cancelList.length
    },
    setNoTreat:function (state,{requestList}) {
        let item={}
        console.log(requestList)

        requestList.map(function (__item) {
            item[__item.id]=__item
            state.notreatList=item
        })

    },

    setTraitors:function (state,{traitorList}) {
        let __item={}
        traitorList.map((item)=> {
            __item[item.id]=item
            state.traitorsList=__item
        })

    },
    command:function (state,{status}) {
       state.commandStatus=status;
    }
};

/**
 * toutes les actions
 */
const actions = {
    /**
     * premiere action récupérer d'abord la liste des requêtes
     */
    loadAllRequest: async function (context) {
        let response = await axios.get('/requestList');
        context.commit('loadRequests', {drafts: response.data.drafts})
    },
    getId: async function (context) {
        let response = await axios.get('/admin/user');
        context.commit('setAuthId', {authId: response.data})
    },
    /**
     * get in queue request
     */
    getInQueue: async function (context) {
        let response = await axios.get('/request/inqueue');
        context.commit('setInQueueRequest', {
            requests: response.data,
        })
    },

    /**
     * @param context
     * @param _formdata
     * @returns {Promise<*>}
     */
    validateRequestBill: async function (context, _formdata) {

        let config = {
            headers: {'content-type': 'multipart/form-data'}
        };
        return await axios.post('/request/inqueue/validate', _formdata, config)
    },
    /**
     *
     */
    loadVRequest:async function (context){
            let response=await axios.get('/request/validate');
            console.log(response.data)
            context.commit('setValidateRequest',{vRequest:response.data})
    },
    /*
    * charge les detail d'une requete en particulier*/

    async loadSingleDetails(context,id) {
            let response=await axios.get('/request/validate/'+id);

            context.commit('setValidateDetails',{validateDetails:response.data})
    },


    /**
     *
     * @param context
     * @param id represents the drafts_id
     * @param comment which represents the reason why he refused the request
     * @returns {Promise<*>}
     */
    cancelRequest:async function(context,{id,comment}){
        return axios.put('/request/cancel/'+id,{
            'comments':comment,
            'is_admin':1,
        })
    },
    loadCancelList:async function(context){
        let cancelLists=await axios.get('/request/cancelist');
        context.commit('setCancelList',{cancelList:cancelLists.data})

    },
    revalidateRequest:async function(context,{requestId,formData}){
        let config = {
            headers: {'content-type': 'multipart/form-data'}
        };
        return  axios.post('/request/revalidate/'+requestId,formData,config)
    },
    notreat:async function(context){

        let response=await axios.get('/axios/request/notreat');
        context.commit('setNoTreat',{requestList:response.data})
    },
    traitors:async function(context){
        let response=await axios.get('/axios/request/traitors');
        console.log(response)
        context.commit('setTraitors',{traitorList:response.data})
    },
    /*passer en mode commande*/
    passCommand:async function(context,id){
        let response=await axios.post('/axios/request/'+id+"/command");
        context.commit("command",{status:response.data})
    }


};
export default {
    state,
    getters,
    mutations,
    actions
}

