import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state={
    clientRequests:{},
    clientOrders:{},
    userOrder:{},
    preOrderedRequest:{},
    payments:{}
};
const getters={
    getClientRequest:function (state) {
        return state.clientRequests
    },
    getClientOrders:function (state) {
        return state.clientOrders
    },
    getClientOrdersDetails:function (state) {
        return state.userOrder
    },
    getAllPreOrder:function (state) {
        return state.preOrderedRequest
    },
    getAllPaymementProcess:function(state){
        return state.payments;
    }
};

const mutations={
    setClientRequest:function (state,{ClRequests}) {
        let request={}

        ClRequests.map(function (__requests) {
            request[__requests.id]=__requests
            state.clientRequest=request
        })
    },
    setAllOrders:function (state,{orders}) {
        let order={}
        orders.map(function (_order) {
           order[_order.id]=_order
            state.clientOrders=order
        })

    },
    setAllUserOrder:function (state,{userOrder}) {
        let _userOrder={}
        userOrder.map(function (order) {
            _userOrder[order.id]=order
            state.userOrder=_userOrder
        })
    },
    getPreOrderedRequest:function (state,{orders}) {
        let _preOrder={}
        orders.map(function (preorder) {
            _preOrder[preorder.id]=preorder
            state.preOrderedRequest=_preOrder
        })
    },
    setAllPaymentProcess:function (state,{orders}) {
        let _order ={}
        orders.map(function (order) {
            _order[order.id_draft]=order
            state.payments=_order
        })
    }
};

const actions={

    loadClients: async function (context) {
        let response= await axios.get('/order/getClient');
        return response;
    },
    emmitPreorder:async function(context,formdata){
        let config = {
            headers: {'content-type': 'multipart/form-data'}
        };
        let response=await axios.post('/order/emmit',formdata,config);
        return response;
    },
    loadPreOrderInqueueRequest:async function(context){
        let response= await axios.get('/order/preordered-list');
        context.commit('getPreOrderedRequest',{orders:response.data})
    },
    loadUserRequest:async function (context,client_id){
        let response=await axios.get('/order/'+client_id)
        context.commit('setAllUserOrder',{userOrder:response.data})
    },

    emmitOrder:async function(context,{id,total}){
        return  await axios.post('/order/'+id+'/emmit',{
            total:total
    })

    },

    loadAllPayementProcess:async function(context){
        let response= await axios.get('/order/payment-processList')
        context.commit('setAllPaymentProcess',{orders:response.data})

    }

};

export default {
    state,
    getters,
    mutations,
    actions
}
