import Vue from "vue";
import Vuex from "vuex";

const state = {
    Lastrequests: [],
    assignedRequestList: [],
    subPaymentList: [],
    clientsList: []
};

const getters = {
    getLastAssignedList: function (state) {
        return state.Lastrequests
    },

    getMyAssignedList: function (state) {
        return state.assignedRequestList
    },
    getSubPaymentList: function (state) {
        return state.subPaymentList
    },
    getClients: function (state) {
        return state.clientsList
    }
};

const mutations = {

    setLastAssignedList: function (state, {list}) {
        let _request = {};
        list.map(function (item) {
            _request[item.id] = item
            console.log(item)
            state.Lastrequests = _request
        })
    },

    setAssignedList: function (state, {list}) {
        let request = {};
        list.map(function (_request) {
            request[_request.id] = _request
            state.assignedRequestList = request;
        })
    },
    setSubPayment: function (state, {list}) {
        let payment = {};
        list.map(function (__item) {
            payment[__item.id] = __item
            state.subPaymentList = payment
        })
    },
    setAllClients: function (state, {list}) {
        let _client = {};
        list.map(function (__client) {
            _client[__client.id] = __client
            state.clientsList = _client
        })
    }
};

const actions = {
    loadLastAssignedRequest: async function (context) {
        let response = await axios.get('/request/lastassignedrequestsList')
        context.commit('setLastAssignedList', {list: response.data})
    },
    /*permet de charger toutes les requetes que je traite */

    loadAllMyRequest: async function (context) {
        let response = await axios.get('/request/myAssignedrequestsList')
        context.commit("setAssignedList", {list: response.data})
    },
    loadSubPayment: async function (context, id) {
        let response = await axios.get('/order/' + id + '/getPayments')
        context.commit('setSubPayment', {list: response.data})
    },
    /*load all users*/
    loadAllClients: async function (context, id) {
        let response = await axios.get('/axios/getUsers');
        context.commit('setAllClients', {list: response.data})
    }
};

export default {
    state,
    getters,
    mutations,
    actions
}
