import Vue from 'vue'
import Vuex from 'vuex'


const state={
    totalTreatRequestCount:0,
    treatRequestList:[],
    totalNoTreatRequestCount:0,
};

const mutations={
        initWidget(state,{list,yesCount,noCount}){
            let request={}
            list.map(function (item) {
                request[item.id]=item
                state.treatRequestList=request
            })
            state.totalTreatRequestCount=yesCount;
            state.totalNoTreatRequestCount=noCount;

        }
};

const getters={

    getNoTreatRequestListCount(){
        return state.totalNoTreatRequestCount
    },
    getTreatRequestListCount(){
        return state.totalTreatRequestCount
    },
    getTreatmentRequestList(){
      return state.treatRequestList
    }

};

const actions={
    loadWidgetDatas:async function (context) {
        let response=await axios.get('/admin/request/treatmentList');
        context.commit('initWidget',{
            list:response.data.treatList,
            yesCount:response.data.treatListCount,
            noCount:response.data.notreatListCount
        })
    }
};


export default {
    state,
    mutations,
    getters,
    actions
}
