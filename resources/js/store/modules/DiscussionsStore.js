import Vue from  'vue'
import Vuex from 'vuex'


Vue.use(Vuex)

const state={

    chats:[],
    chatdiscussionsList:[]
};

const mutations={


    setDiscussions:function (state,{discussions}) {
        let _discussion={}

        discussions.map(function (discussion) {
            _discussion[discussion.id]= discussion
            state.chatdiscussionsList = _discussion

        })

    },
    setSingleDiscussion(state,{discussions}){
        let singleChat= {}
        discussions.map(function (_chat) {
            singleChat[discussions.id] = _chat
            state.chats = singleChat
        })

    }

};

const getters={
    getConversation:function (state) {
        return state.chats
    },
    getDiscussions:function (state) {
        return state.chatdiscussionsList
    }
};

const actions={

    async respondeToClient(context,formdata) {
        return await axios.post('/request/accept',formdata)
    },

    loadSingleConversation:async function (context,draft_id) {
        let response=await axios.get('/discussions/load/'+draft_id)
        context.commit('setChatUsers',{setSingleChat:response.data})
    },

    fetchDiscussionContent:async function(context,{draft_id}){
        let response=await axios.get('/discussions/load/'+draft_id)
         console.log(response)

        context.commit('setDiscussions',{discussions:response.data})
    },
    respondeToUser:function (context,_formdata) {
        let admin_id=_formdata.get('admin_id')

        return axios.post('/discussions/load/'+admin_id+'/messages',_formdata);

    },
    getAllRequestSubject:async function(context){
        let response =  await axios.get('/discussions/admin/retreiveChatSubject')

        context.commit("setDiscussions",{discussions:response.data})
        // context.commit("")
    }

};

export default {
    state,
    mutations,
    getters,
    actions
}
