/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap')




import Vue from 'vue'
import Axios from "axios";
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import Store from './store/store'
import Toasted from 'vue-toasted'
import toast from 'egalink-toasty.js'
import * as Firebase from "firebase/app";
import VueNoty from "vuejs-noty"
import 'vuejs-noty/dist/vuejs-noty.css'

Vue.use(VueNoty)


window.io = require('socket.io-client');


Vue.prototype.$http = Axios;


Vue.use(VueRouter)
Vue.use(Toasted,{
    duration:3000,
})

import Echo from "laravel-echo";

let echo = new Echo({
    broadcaster: 'socket.io',
    host:window.location.hostname+':6001'
})



Vue.prototype.$echo = echo





// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
import home from './components/HomeComponent'
import RequestComponent from './components/RequestComponents'
import SingleRequestComponent from './components/manager/request/SingleRequestComponent'
import RequestValidate from "./components/manager/request/RequestValidate";
import RequestInQueue from "./components/Admin/request/RequestInQueue";
import preOrderComponent from "./components/manager/order/preOrderComponent";
import ChatComponent from "./components/manager/chat/chatComponent"
import SingleChat from "./components/manager/chat/SingleChat";
import DiscussionComponentInactive from "./components/Admin/ChatSubComponent/DiscussionComponentInactive";
import RequestCancel from "./components/manager/request/RequestCancel";
import SingleOrderComponent from "./components/manager/order/SingleOrderComponent";
import AssignRequest from "./components/Admin/request/assignRequest";
import myAssignView from "./components/manager/request/MyAssignRequest"


import ManagerHome from "./components/manager/Home"
import ManagerAssignRequest from "./components/manager/request/MyAssignRequest";
import NotificationComponent from "./components/manager/other/Notification";
import conf from "./components/confidentiality"
import Fournisseur from "./components/Fournisseur";
import RequestTreatment from "./components/Admin/request/RequestTreatment";
import AppSlide from "./components/AppSlide";


let routes = [
    {path: '/home', component: home, name: 'Home'},
    {path: '/policy', component: conf, name: 'Policy'},
    {path: '/', component: home},
    {path:'/admin/myAssign',component:myAssignView,name:'AssignedRequestView'},
    {
        path: '/admin/requetes/assign', component: AssignRequest,
        name:'AssignRequestView'

    },
    {
        path: '/admin/requetes', component: RequestComponent,
        name:'RequestHome'

    },
    {
        path:'/admin/requetes/inQueue',
        component:RequestInQueue,
        name:'InQueueRequest'
    },
    {
        path:'da/:url',
        name:'AttachedFile',
    },
    {
        path:'/admin/request/validate',
        component:RequestValidate,
        name:'RequestValidate',

    },
    {
      path:'/admin/request/cancel',
        component:RequestCancel,
        name:'RequestCancel'
    },
    {
        path:'/admin/preorder',
        component : preOrderComponent,
        name:'preOrder'
    },
    {
        path:'/admin/discuss',
        component:ChatComponent,
        name:'chatComponent',
        /*children path for the chat component */
        children:[
            {
                path:'/admin/discuss/:id',
                component:SingleChat,
                props: true,
                name:'chat'
            },
            {
                path: '/admin/discuss',
                component:DiscussionComponentInactive,

            }
        ]
    },
    {
        path:'/admin/preorder/user/:client_id',
        name:'userPreOrder',
        component:SingleOrderComponent
    },

    /*route for single manager*/
    {
        path:"/manager/home",
        name:"_managerHome",
        component:ManagerHome
    },
    {
        path:"/manager/myRequests",
        name:"_managerAssignedRequest",
        component:ManagerAssignRequest
    },
    {
        path: '/manager/requete/:id',
        component: SingleRequestComponent,
        name: 'SingleRequest'
    },
    {
        path:'/manager/notification',
        component:NotificationComponent,
        name:'_notification'
    },
    {
        path:'/fournisseur',
        component:Fournisseur,
        name:'_fournisseur'
    },
    {
        path:'/admin/request/treatlist',
        component:RequestTreatment,
        name:'requestTreatmentList'
    },
    {
        path:'/admin/slides',
        component:AppSlide,
        name:'_slidePage'
    }

];

let router = new VueRouter(
    {
        mode: 'history',
        routes
    }
);
/**;
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    store: Store
});
