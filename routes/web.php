<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/order', 'AdminController@getOrderedUsers');

Route::post('/axios/request/{id}/command','OrdersController@prepareCommand');
/*partie draft*/
Route::get('/lastFiveRequest', 'DraftsController@showLastFiveDrafts');
Route::get('/axios/request/traitors', 'AdminController@getTraitors');
Route::get('/requestList', 'RequetesController@getRequestList');
Route::patch('/draft/{id}/attach','DraftsController@setInProcesssing');
Route::put('/axios/request/assign','DraftsController@attach');

Route::post('/sendNotification','OrdersController@sendBillToUser');
Route::post('/manager/responde/{id}','DraftsController@respondeToUser');

/*fin partie draft*/
/*PArtie requête et on verifie si*/

Route::get('/order/payment-processList', 'OrdersController@getAllPaymentProcess');
Route::get('/draft/details/{id}', 'DraftsController@showSingleRequest');

Route::get('/request/validate/admin', 'AdminController@getAdmins');
Route::post('/requete/{id}', 'DraftsController@sendForValidation');
Route::get('/admin/user', 'HomeController@getAuthUser');
Route::get('/admin/myRequest','DraftsController@getMyRequests');
/*
 *partie reqûete
 */
Route::get('/request/inqueue','DraftsController@getInQueueRequest');
Route::get('/request/inqueue/attach','RequetesController@getRequestWithAttachments');
Route::post('/request/inqueue/validate','DraftsController@ValidateBill');
Route::get('/request/validate','DraftsController@getValidateRequest');
Route::get('/axios/getUsers','AdminController@getAllClient');
Route::get('/order/preordered-list/','OrdersController@listPreorderedRequest');

Route::get('/request/validate/{id}','RequetesController@getSingleValidateDetails');
Route::put('/request/cancel/{id}','DraftsController@CancelRequest');
Route::post('/request/revalidate/{id}','DraftsController@revalidateRequest');
Route::get('/request/cancelist','AdminController@getMyCancelRequestList');
Route::post('/request/accept','RequetesController@acceptRequest');
Route::get('/send','RequetesController@sendNotification');
Route::post('/admin/notify/{token}/push','AdminController@notifyByPush');
Route::post('/admin/notify/{user}/sms','AdminController@notifyBySms');
Route::post('/admin/fournisseur/import','AdminController@importFile');
Route::get('/admin/fournisseur/list','AdminController@getFournisseurList');
Route::get('/admin/request/treatmentList','AdminController@getTreatRequestList');





/*
 * partie reponse
 */



/*
 * partie precommande
 */
Route::get('/order/getClient','ClientController@getClientValidateRequest');
Route::get('/order/getClientRequest/{id}','AdminController@getRequestByClient');
Route::post('/order/{id}/emmit','OrdersController@EmmitPurchase');
Route::get('/order/all','OrdersController@getOrders');
Route::get('/order/{client_id}','OrdersController@getOrderDetails');
Route::get('/axios/request/notreat', 'DraftsController@noTreat');



/*
 *partie chat et discussions
 */
Route::get('/discussions/load/{draft_id}','DiscussionsController@loadSingleChatDetails');
Route::get('/discussions/load/{request_id}/messages','DiscussionsController@getUserMessageByIdAdminPart');
Route::post('/discussions/load/{request_id}/messages','DiscussionsController@adminRespondeToUser');


Route::get('/discussions/admin/retreiveChatSubject','DiscussionsController@adminRetreiveAllMessages');

Route::get('/request/lastassignedrequestsList',"AdminController@getMyLastTenAssignedRequest");
Route::get('/request/myAssignedrequestsList',"AdminController@getAllMyAssignedRequest");






Route::get('/order/{id}/getPayments','OrdersController@getSubpayments');
Auth::routes();

Route::get('/logout',function (\Illuminate\Http\Request $request,\Illuminate\Http\Response $response){
   \Illuminate\Support\Facades\Auth::guard('admin')->logout();
   return redirect()->route('login');
});

Route::get('/policy',function (\Illuminate\Http\Request $request,\Illuminate\Http\Response $response){
   return view('policy');
});



/*single manager route part*/

Route::delete('/admin/fournissseur/{id}/delete','AdminController@deleteFournisseur');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/{path}', 'HomeController@index')->where('path', '.*');

});


