<?php

use Illuminate\Http\Request;
use Illuminate\Http\Response;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('/initdraft/sendAttachments','DraftsController@attachFiles');
Route::post('/register','ClientController@register');
Route::get('/\quest/{id}', 'ClientController@getRequest');
Route::get('/myRequest/last/{id}', 'ClientController@getMyLastRequest');
Route::get('/myRequest/{client_id}/request/{request_id}/detail', 'ClientController@getRequestDetails');
Route::get('/verify/{telephone}','ClientController@checkExistingUser');
Route::post('/initDraft','DraftsController@initDraft')->name('draft.save');
Route::get('/myRequest/{customer_id}/discussion/{request_id}','DiscussionsController@getDiscussions');
Route::post('/myRequest/response','DiscussionsController@Responde');
Route::post('/user/{id}/saveToken','ClientController@SaveToken');
Route::get('/request/{id}/getInfo','RequetesController@getAcceptance');
Route::patch('/request/{id}/sayOk','ClientController@sayOk');
Route::post('/request/{id}/sayNo','ClientController@sayNo');
Route::get('/discussions/Lists/{client_id}','DiscussionsController@getAllDiscussions');
Route::post('/update/token/{id}','ClientController@updateToken');
Route::post('/pay/request/init','OrdersController@initPayment');
Route::post('/request/{purchase_id}/pay','OrdersController@proceedToPay');
Route::get("/{draft_id}/checkPay","OrdersController@checkExistingPaymentProcess");
Route::post('/payment/{sub_id}/updatestatus','OrdersController@updateStatus');



//test API/api/pay/request/init
Route::get('/admin/myRequest','DraftsController@getMyRequests');


/*for the admin*/



Route::post("/admin/login","ApiAdminController@logUser");
Route::get("/admin/request/all","ApiAdminController@getLastRequest");
Route::get("/admin/{id}/request","ApiAdminController@getMyRequestList");

Route::get('/admin/request/treatList',"ApiAdminController@getTreatList");

Route::post('/request/setVisible','DraftsController@setReadable');
Route::get('/admin/request/{id}/details','ApiAdminController@getSingleDetails');

Route::post('/slide/changes','ApiAdminController@updateCaroussel');
Route::get('/loadSlides','ApiAdminController@loadSlides');


