<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discussion extends Model
{
    //

    public $timestamps=false;

    protected $fillable=['from','content','from_id','discuss_at','to_id','request_id'];

    protected $dates=['discuss_at'];

    public function fromId(){
        return $this->belongsTo(User::class,"from_id");
    }
    public function toId(){
        return $this->belongsTo(User::class,"to_id");
    }

}
