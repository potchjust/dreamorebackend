<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    public $timestamps = false;
    protected $fillable = ['lname', 'fname', 'address', 'email', 'telephone','country_code','fcm_token'];

    public function user()
    {
        return $this->morphMany(User::class, 'user');
    }

    public function drafts()
    {
        return $this->hasMany(Drafts::class);
    }

    public function discussions(){
        return $this->hasMany(Discussion::class);
    }

    public function requetes(){
        return $this->belongsToMany(Requete::class);
    }
}
