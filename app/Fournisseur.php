<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use function GuzzleHttp\Promise\all;

class Fournisseur extends Model implements ToModel ,FromCollection
{
    //

    public $timestamps=false;
    protected $fillable=["denomination","contact"];

    /**
     * @param array $row
     *
     * @return Model|Model[]|null
     */public function model(array $row)
        {
          return new Fournisseur([
              'denomination'=>$row[0],
              'contact'=>$row[2]
          ]);


        }

    /**
     * @return Collection
     */
    public function collection()
    {
        return self::all();
    }
}
