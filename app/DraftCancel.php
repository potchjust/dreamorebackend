<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DraftCancel extends Model
{
    //

    protected $fillable=["bill_url",'cancel_by','draft_id','reason','is_admin'];
    public $timestamps=false;

    public function draft(){
        return $this->belongsTo(Drafts::class);
    }

    public function cancelBy(){
        return $this->belongsTo(User::class);
    }
}
