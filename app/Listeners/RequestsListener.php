<?php

namespace App\Listeners;

use App\Events\ShowRequestReceivedNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ShowRequestReceivedNotification $event
     * @return void
     */
    public function handle(ShowRequestReceivedNotification $event)
    {

    }
}
