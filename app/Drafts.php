<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drafts extends Model
{
    //
    protected $fillable = [
        'draft_subject',
        'draft_quantity',
        'draft_content',
        'draft_deliver_mode',
        'client_id',
        'received_at',
        'process_at',
        'admin_id',
        'state',
        'to_validate',
    ];
    public $timestamps = false;

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

    public function draftsCancels(){
        return $this->hasMany(DraftCancel::class);
    }
    public function draftsValidates(){
        return $this->hasMany(DraftValidate::class);
    }

    public function validate(){
        return $this->belongsTo(Admin::class);
    }

    public function purchase(){
        return $this->belongsTo(Purchase::class);
    }
}
