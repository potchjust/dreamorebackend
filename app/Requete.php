<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requete extends Model
{
    //
    protected $fillable = ['request_date', 'statut','draft_id','from_id','to_id','comment'];
    protected $dates = ['request_date'];
    public $timestamps = false;


    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

    public function clients(){
           return $this->belongsToMany(Client::class);
    }
    /**
     * Récuperer plus facilement la liste des requêtes qui ont des fichiers attaché
     */




}
