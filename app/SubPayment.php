<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubPayment extends Model
{
    //
    protected $fillable=['paid','purchase_id','paid_at','rest','identifier','payment_method','status'];
    public $timestamps=false;
    protected $dates=['paid_at'];

    public function payment(){
        return $this->belongsTo(Payment::class);
    }
}
