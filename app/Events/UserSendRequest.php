<?php

namespace App\Events;

use App\Drafts;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserSendRequest implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Drafts
     */
    public $drafts;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Drafts $drafts)
    {
        //
        $this->drafts = $drafts;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {

        return new PrivateChannel('drafts');
    }




    public function broadcastWith(){
        return [
            "draftId"=>$this->drafts->id
        ];
    }


}
