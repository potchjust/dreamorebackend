<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    //
    public $timestamps=false;

    protected $fillable=['draft_id', 'total_price','state'];


    public function attachments(){
        return $this->morphMany(Attachment::class,'attachable');
    }

    public function drafts(){
        return $this->hasMany(Drafts::class);
    }

    public function payment(){
        return $this->hasOne(Payment::class,'purchase_id');
    }
}
