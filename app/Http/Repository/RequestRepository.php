<?php
/**
 * classe devrant servir de classe de requêtage
 */

namespace App\Http\Repository;;

use App\Client;
use App\Drafts;
use App\Requete;
use App\User;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

class RequestRepository
{

    /**
     * @var AuthManager
     */
    private $authManager;

    public function __construct(AuthManager $authManager)
    {
        $this->authManager = $authManager;
    }

    public function showAllRequests()
    {
        $requests = Drafts::select('drafts.id', 'draft_subject', 'client_id', 'received_at', 'lname', 'admin_id')
            ->join('clients', 'client_id', 'clients.id')
            ->orderBy('received_at', 'desc')
            ->get();

        return $requests;
    }

    public function showLastFiveRequests()
    {
        $requests = Drafts::select('drafts.id', 'draft_subject', 'client_id', 'received_at', 'lname', 'admin_id')
            ->join('clients', 'client_id', 'clients.id')
            ->orderBy('received_at', 'desc')
            ->limit(5)
            ->get();

        return $requests;
    }

    public function showSingleRequest(int $id)
    {
        $request = Drafts::select('drafts.id', 'draft_subject', 'draft_quantity', 'draft_content', 'draft_deliver_mode',
            'client_id','drafts.state',
            'received_at', 'lname', 'fname', 'clients.address', 'telephone', 'admin_id','purchases.id as id_purchase','purchases.total_price','purchases.purchase_state')
            ->join('requetes','requetes.draft_id','drafts.id')
            ->join('clients', 'client_id', 'clients.id')
            ->leftjoin('purchases','purchases.draft_id','drafts.id')
            ->where('drafts.id', '=', $id)
            ->orderBy('received_at', 'DESC')
            ->get();

        return $request;
    }

    public function getAttachments(int $id)
    {
        $draft = Drafts::find($id);
        $attachments = $draft->attachments()->get();
        if ($attachments===null){
            return null;
        }
        return $attachments;
    }

    public function IsRequest(int $id)
    {
        $requete = Requete::select('draft_id', 'statut')
            ->where('draft_id', '=', $id)
            ->first();
        return $requete;
    }

    /**
     * @return mixed on se base sur le statut pour recupérer les requetesw qui sont en attentes
     * si le statut est 0 =en attente de validation
     * 1 validé
     * -1 rejeté
     */

    public function getRequestsInQueue()
    {
        $requests = Drafts::select(
            'drafts.id',
            'drafts.state',
            'drafts.draft_quantity',
            'admins.username',
            'drafts.draft_content')
            ->join('admins', 'requetes.from_id', 'admins.id')
            ->where('drafts.state', '=', 2)
            ->get();
        return $requests;
    }

    /**
     * @return mixed validated requests
     */
    public function getValidatedRequests()
    {
        $requests = Requete::select('requetes.id','requetes.statut','clients.lname','drafts.client_id','clients.fname','drafts.draft_subject', 'admins.username','attachment_url')
            ->join('drafts', 'drafts.id', 'requetes.draft_id')
            ->join('admins', 'requetes.from_id', 'admins.id')
            ->join('clients','clients.id','drafts.client_id')
            ->join('attachments','attachments.attachable_id','requetes.id')
            ->where('requetes.statut', '=', 1)
            ->get();
        return $requests;
    }

    /**
     * @return mixed rejected requests
     */
    public function getRejectedRequests()
    {
        $requests = Requete::select('requetes.id','requetes.to_id','requetes.statut', 'drafts.draft_subject', 'admins.username','requetes.comment','requetes.cancel_by')
            ->join('drafts', 'drafts.id', 'requetes.draft_id')
            ->join('admins', 'requetes.from_id', 'admins.id')
            ->where('requetes.statut', '=', -1)
            ->Orwhere('requetes.statut', '=', -3)
            ->get();
        return $requests;
    }

    /*
     * requetes en attente de validation par
     * l'admin
     */
    public function getRequestWithAttachments()
    {
        $requests = Requete::select('requetes.id','requetes.statut', 'drafts.draft_subject', 'admins.username','attachment_url','attachments.file_extension')
            ->join('drafts', 'drafts.id', 'requetes.draft_id')
            ->join('admins', 'requetes.from_id', 'admins.id')
            ->join('attachments','attachments.attachable_id','requetes.id')
            ->where('requetes.statut', '=', 0)
            ->where('requetes.to_id', '=',$this->authManager->guard()->user()->id)
            ->get();
        return $requests;
    }

    public function getSingleDetails($id){
        return Drafts::select('drafts.draft_subject')
            ->join('clients','clients.id','drafts.client_id')
            ->get();
    }

    public function getUserByRequestId($id){
        return Drafts::select('drafts.id','users.id as client_id')
            ->join('users','users.id','drafts.client_id')
            ->where('drafts.id',$id)
            ->first();
    }

}
