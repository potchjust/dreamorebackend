<?php


namespace App\Http\Repository;;


use App\Drafts;
use App\Order;
use App\Purchase;
use Illuminate\Auth\AuthManager;

class OrderRepository
{
    private $authManager;
    public function __construct(AuthManager $manager)
    {
        $this->authManager=$manager;
    }

    public function loadPreorderInqueueRequest(){
        return Drafts::select('drafts.id', 'draft_subject', 'client_id',
            'received_at', 'lname', 'admin_id',
            'drafts.draft_deliver_mode','drafts.state')
            ->join('clients', 'client_id', 'clients.id')
            ->where('admin_id','=',$this->authManager->guard()->user()->id)
            ->where('drafts.state','=',5)
            ->get();

    }

    public function getAllPaymentProcess(){
        return Purchase::select('purchases.total_price','purchases.purchase_state',
                'drafts.id as id_draft',
                'clients.id as id_client',
                'clients.lname',
                'clients.fname',
                'drafts.state',
                'drafts.draft_subject')
            ->join('drafts','drafts.id','purchases.draft_id')
            ->join('clients','clients.id','drafts.client_id')
            ->groupBy('drafts.id')
            ->where('drafts.admin_id',$this->authManager->guard()->user()->id)
            ->get();
    }

}
