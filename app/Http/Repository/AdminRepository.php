<?php
/**
 * Get all the request concerning the admin
 */


/*TODO get the files send by validation */
namespace App\Http\Repository;;

use App\Admin;
use App\Attachment;
use App\Client;
use App\Drafts;
use App\Requete;
use http\Env\Request;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Response;

class AdminRepository
{
    /**
     * @var Guard
     */
    private $guard;
    /**
     * @var AuthManager
     */
    private $authManager;

    /**
     *Récupérer la liste des administrateur différent de l'utilisateur connecté
     * @param Guard $guard
     * @param AuthManager $authManager
     */

    public function __construct(Guard $guard,AuthManager $authManager)
    {
        $this->guard = $guard;
        $this->authManager = $authManager;
    }

    public function getOtherAdmins()
    {
        $admins = Admin::where('role_id','=','1')
            ->where('id','!=',$this->guard->user()->id)
            ->get();
        return $admins;
    }

    public function getMyLastTenAssignedRequest(){
        $drafts=Drafts::select('drafts.id','drafts.draft_content','clients.fname','clients.lname','drafts.state','drafts.draft_subject')
            ->join("clients",'drafts.client_id','=','clients.id')
            ->where("admin_id",'=',$this->guard->user()->id)
            ->limit(10)
            ->get();
        return $drafts;
    }

    public function getAllMyAssignedRequest(){

        $drafts=Drafts::select('drafts.id','drafts.draft_content','clients.fname','clients.lname','drafts.state','drafts.draft_subject')
            ->join("clients",'drafts.client_id','=','clients.id')
            ->where("admin_id",'=',$this->authManager->guard()->user()->id)
            ->get();
        return $drafts;
    }

    public function getAllMyAssignedRequestByApi($id){
        $drafts=Drafts::select('drafts.id','drafts.draft_content','clients.fname','clients.lname','drafts.state','drafts.draft_subject')
            ->join("clients",'drafts.client_id','=','clients.id')
            ->where("admin_id",'=',$id)
            ->get();
        return $drafts;
    }

    public function getMyCancelRequestList(){
        $drafts=Drafts::select('drafts.id','drafts.draft_content','reason','admins.username','clients.fname','clients.lname','drafts.state','drafts.draft_subject')
            ->leftjoin("clients",'drafts.client_id','=','clients.id')
            ->leftjoin('draft_cancels','drafts.id','draft_cancels.draft_id')
            ->leftjoin('admins','draft_cancels.cancel_by','admins.id')
            ->leftjoin('users','users.user_id','draft_cancels.cancel_by')
            ->where("drafts.admin_id",'=',$this->authManager->guard()->user()->id)
            ->where('drafts.state',-3)
            ->get();
        return $drafts;
    }

    public function getRequestByClient($id)
    {
        $draft=Drafts::select('drafts.id','drafts.draft_subject')
            ->join('clients','clients.id','drafts.client_id')
            ->where('drafts.state','=',4)
            ->where('drafts.admin_id','=',$this->authManager->guard()->user()->id)
            ->where('clients.id','=',$id)
            ->get();
        return \response()->json(['request'=>$draft]);
    }


    public function getOrderedClients($client){
        $client=Client::select('clients.fname','clients.lname','clients.id')
            ->join('drafts','drafts.client_id','clients.id')
            ->where('drafts.state','=',4)
            ->where('clients.lname','=',$client)
            ->groupBy('clients.id')
            ->get();
        return $client;
    }

    public function getRequests()
    {
        $requests = Drafts::select('drafts.id', 'draft_subject', 'client_id', 'received_at', 'lname', 'admin_id','drafts.draft_deliver_mode','drafts.state')
            ->join('clients', 'client_id', 'clients.id')
            ->orderBy('received_at', 'desc')
            ->get();

        return $requests;
    }


    public function getAllTreatRequest(){
        return  Drafts::select('drafts.id', 'draft_subject', 'client_id', 'received_at', 'lname', 'admin_id','drafts.draft_deliver_mode','drafts.state')
            ->join('clients', 'client_id', 'clients.id')
            ->join('admins','drafts.admin_id','admins.id')
            ->where('drafts.admin_id','!=',0)
            ->get();
    }


    public function getApiSingleRequestDetails($id){

        $row=[
            'drafts.id',
            'draft_subject',
            'client_id',
            'received_at',
            'clients.lname',
            'clients.address',
            'clients.fname',
            'clients.telephone',
            'clients.country_code',
            'admin_id',
            'drafts.draft_quantity',
            'drafts.draft_content',
            'drafts.draft_deliver_mode',
            'drafts.state',
            'admins.username',
            'admins.id as _adminId'
        ];



        /*get the single drafts details*/
        $draft=Drafts::leftjoin('admins',function ($join){
            $join->on('admins.id','drafts.admin_id');
        })->join('clients', 'drafts.client_id', 'clients.id')
            ->where('drafts.id','=',$id)
            ->first($row);

        /*get the attachments send by the client*/
        $attached=Attachment::join('drafts','drafts.id','attachments.attachable_id')
                    ->where('attachments.attachable_id',$draft->id)
                    ->where('attachments.is_attached',1)
                    ->get([
                        'attachments.name',
                        'attachments.attachment_url',
                        'attachments.attachable_id',
                        'attachments.file_extension',
                        'attachments.is_attached'
                    ]);


        /*return of the data*/
       return  [
           'draft'=>$draft,
           'attachments'=> $attached

       ];



    }

}
