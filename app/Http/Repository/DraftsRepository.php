<?php


namespace App\Http\Repository;;


use App\Drafts;
use App\DraftValidate;
use App\Requete;
use App\SubPayment;
use Carbon\Carbon;
use Illuminate\Auth\AuthManager;

class DraftsRepository
{

    private $drafts;
    /**
     * @var AuthManager
     */
    private $authManager;

    public function __construct(Drafts $drafts,AuthManager $authManager)
    {

        $this->drafts = $drafts;
        $this->authManager = $authManager;
    }

    /**
     * recupérer les 5 derniers brouillons  arrivé dans la boite
     */

    public function getLastFiveDrafts()
    {
        $lastDrafts=$this->drafts->select('drafts.id', 'draft_subject', 'client_id', 'received_at', 'lname', 'admin_id')
            ->join('clients','clients.id','drafts.client_id')
            ->orderBy('received_at','DESC')
            ->limit(5)
            ->get();
        return $lastDrafts;
    }
    public function createDraft($datas)
    {

        $draft =$this->drafts->create([
            'draft_subject' => $datas['draft_subject'],
            'draft_quantity' => $datas['draft_quantity'],
            'draft_content' => $datas['draft_content'],
            'draft_deliver_mode' => $datas['draft_deliver_mode'],
            'client_id' => $datas['client_id'],
            'received_at' => Carbon::now(),
            'process_at' => Carbon::create(1970, 01, 01, 0, 0, 0),
            'state'=>0
        ]);

        if ($draft) {
           return $draft;
        } else {
            return null;
        }
    }

    public function getValidatedRequests()
    {
        $requests=DraftValidate::select('attachments.name','attachments.attachment_url','drafts.draft_subject','drafts.id','clients.fname','clients.lname','drafts.client_id','admins.username')
            ->join('drafts','drafts.id','draft_validates.draft_id')
            ->join('clients','clients.id','drafts.client_id')
            ->join('admins','draft_validates.approved_by','admins.id')
            ->join('attachments','attachments.attachable_id','drafts.id')
            ->where('attachments.validation','=',3)
            ->where('drafts.state','=',3)
            ->where('drafts.admin_id',$this->authManager->guard()->user()->id)
            ->get();
        return $requests;
    }

    public function showSingleRequest(int $id)
    {
        $request = Drafts::select('drafts.id', 'draft_subject', 'draft_quantity', 'draft_content', 'draft_deliver_mode',
            'client_id','drafts.state',
            'received_at', 'lname', 'fname', 'clients.address', 'telephone', 'admin_id')
            ->join('clients', 'client_id', 'clients.id')
            ->where('drafts.id', '=', $id)
            ->orderBy('received_at', 'DESC')
            ->first();

        return $request;
    }


    public function getDraftsPaymentProcess(int $draftId){
        return SubPayment::select('sub_payments.id','sub_payments.paid','sub_payments.purchase_id',
                                            'sub_payments.paid_at','sub_payments.rest','sub_payments.identifier',
                                            'sub_payments.payment_method','sub_payments.status','sub_payments.purchase_id',
                                            'purchases.draft_id','purchases.purchase_state','purchases.total_price')
            ->join('purchases','sub_payments.purchase_id','purchases.id')
            ->join('drafts','drafts.id','purchases.draft_id')
            ->where('purchases.draft_id',$draftId)
            ->get();
    }

    public function getAttachments(int $id)
    {
        $draft = Drafts::find($id);
        $attachments = $draft->attachments()->where('is_attached','=',1)->get();
        if ($attachments===null){
            return null;
        }
        return $attachments;
    }

}
