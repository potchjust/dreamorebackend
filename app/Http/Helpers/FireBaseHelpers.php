<?php


namespace App\Http\Helpers;
//dsasd

use App\Drafts;
use Kreait\Firebase;
use Kreait\Firebase\Messaging\ApnsConfig;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;

class FireBaseHelpers
{




    public static function sendNotification ($bill_request_id,$token,$content=false){
        $file=__DIR__.'/dreammoreappnew.json';
        $serviceAccount=Firebase\ServiceAccount::fromJsonFile($file);
        $messaging = (new Firebase\Factory())
            ->withServiceAccount($serviceAccount)
            ->createMessaging();

        echo $content;

        if ($content){
            $config = ApnsConfig::fromArray([
                'headers' => [
                    "apns-push-type"=> "alert",
                    "apns-priority"=> "5"
                ],

                'payload' => [
                    'aps' => [
                        'alert' => [
                            'title' => 'Dreammore',
                            'body' => $content
                        ],
                        "contentAvailable" => 1

                    ],
                ],
            ]);

        }else{
            $config = ApnsConfig::fromArray([
                'headers' => [
                    "apns-push-type"=> "alert",
                    "apns-priority"=> "5"
                ],

                'payload' => [
                    'aps' => [
                        'alert' => [
                            'title' => 'Dreammore',
                            'body' => 'Votre facture est prête',
                        ],
                        "content_available"=>1,
                    ],
                ],
            ]);

        }


        if ($content){
            $message = CloudMessage::withTarget('token',$token)
               ->withApnsConfig($config)
                ->withData([
                    'request_id'=>$bill_request_id,
                    'content'=>$content
                ]);
        }else{
            $message = CloudMessage::withTarget('token',$token)
                ->withApnsConfig($config)
                ->withData(['request_id'=>$bill_request_id]);
        }

        try {
             $messaging->send($message);
        } catch (Firebase\Exception\MessagingException $e) {
            echo $e->getMessage();
        }
    }

    public static function sendSingleNotification ($token,$_message){
        $file=__DIR__.'/dreammoreappnew.json';
        $serviceAccount=Firebase\ServiceAccount::fromJsonFile($file);
        $messaging = (new Firebase\Factory())
            ->withServiceAccount($serviceAccount)
            ->createMessaging();


        $message = CloudMessage::withTarget('token',$token)
            ->withNotification(Notification::create('Information', $_message,null));
        try {
            $messaging->send($message);
        } catch (Firebase\Exception\MessagingException $e) {
            echo $e->getMessage();
        }
    }
}
