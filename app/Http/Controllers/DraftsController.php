<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\DraftCancel;
use App\Drafts;
use App\DraftValidate;
use App\Events\RequestEvent;
use App\Events\UserSendRequest;
use App\Http\Helpers\FireBaseHelpers;
use App\Http\Repository\DraftsRepository;
use App\Requete;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use function GuzzleHttp\Psr7\str;

class DraftsController extends Controller
{
    /**
     * @var DraftsRepository
     */
    private $draftsRepository;
    /**
     * @var AuthManager
     */
    private $authManager;
    /**
     * @var Guard
     */
    private $guard;

    private $redis;

    /**
     * controller se chargeant de s'occuper des brouillons vu qu'avant de devenir
     * une requête elle était avant tout un brouillon
     * Donc on aura
     * une methode permeettant de listers les  derniers brouillon reçu
     * PERMETTANT DE CONSULTER UN BROUILLON
     * DE CHANGER SON STATUT EN EN COURS DE TRAITEMETN
     * DE LE TRANSFORMER EN REQUETE
     * @param DraftsRepository $draftsRepository
     * @param AuthManager $authManager
     */

    public function __construct(DraftsRepository $draftsRepository, AuthManager $authManager, Guard $guard)
    {
        $this->draftsRepository = $draftsRepository;
        $this->authManager = $authManager;
        $this->guard = $guard;
        $this->redis = Redis::connection();
    }

    /**
     * liste toutes les requête
     */
    public function index()
    {

    }

    /**
     * liste les 5 dernières requêtes
     * @param Request $request
     * @param Response $response
     */
    public function showLastFiveDrafts(Request $request, Response $response)
    {
        return \response()->json([
            'drafts' => $this->draftsRepository->getLastFiveDrafts()
        ]);
    }

    /**
     * Marque un brouillon comme en cours de traitement
     * @param Request $request
     * @param Response $response
     */

    public function setInProcesssing(Request $request, Response $response, $id)
    {

        $admin_id = $request->get('user_id');

        $draft_update = Drafts::where('id', $id)->update([
            'process_at' => Carbon::now(),
            'admin_id' => $admin_id
        ]);
        if ($draft_update) {
            return \response()->json([
                'code' => 200
            ]);
        } else {
            return \response()->json(['code' => 600]);
        }
    }

    /**
     * TODO refactorer vers le repository
     * creation du draft qui est en le truc qui est le draft
     * @param Request $request
     * @param Response $response
     * l'utilsateur envoie sa demande
     * cette demande se présente de cette façon
     * ->le sujet de la reqûête
     * ->la quantité demandé
     * ->le contenu
     * ->le mode de livraison
     *  ->un tableau de fichiers si existe
     * @return \Illuminate\Http\JsonResponse
     */
    public function initDraft(Request $request, Response $response)
    {
        /**
         * d'abord on verifie si il y'a un fichier attaché
         */
        if ($request->file('attachment')) {
            $files = $request->file('attachment');
            $draft = $this->draftsRepository->createDraft($request->all());

            /***
             * si on recupère bien un objet de type draft
             * alors on verifie si il y'a des fichiers attachés
             *Si oui on les attache
             * si l'attachage marche alors retourne un statut code de type 200
             */
            if ($draft != null) {

                return $draft;
            } else {
                return \response()->json([
                    'code' => 600,
                    'message' => 'Erreur lors de l\'insertion'
                ]);
            }

        } else {
            $draft = $this->draftsRepository->createDraft($request->all());
             broadcast(new UserSendRequest($draft));


            if ($draft) {
                $client_id = $draft->client_id;
                $client = \App\Client::find($client_id);
                $requete = Requete::create([
                    'request_date' => Carbon::now(),
                    'statut' => -5,
                    'draft_id' => $draft->id
                ]);
                return $draft;
            } else {
                return null;
            }
        }
    }


    public function attachFiles(Request $request, Response $response)
    {


        /**
         * si la requete a des fichiers attaches
         * alors la effectuer l'operation
         */

        if ($request->hasFile('attachment')) {

            $id = $request->get('draft_id');
            $draft = Drafts::find($id);
            $files = $request->file('attachment');

            /*
             * on boucle sur tous les fichiers
             * et on recupere l'extension
             */

            if (is_array($files)) {
                foreach ($files as $file) {
                    $fileExtension = $file->getClientOriginalExtension();
                    if ($fileExtension === 'mp3') {
                        $fileName = 'Audio';
                    } else {
                        $fileName = 'image';
                    }

                    $upload = Storage::disk('public')->put('uploads/apps', $file);
                    $path = Storage::url($upload);

                    /**
                     * si l'upload a marche alors la sauvegarder les infos dans la base de donneed
                     */
                    if ($upload) {
                        $attach = $draft->attachments()->create([
                            'name' => $fileName,
                            'file_extension' => $fileExtension,
                            'attachment_url' => $path,
                            'is_attached'=>1
                        ]);
                        if ($attach) {
                            return \response()->json(['code' => 200]);
                        }
                    } else {
                        /* vue l'export n.a pas marché supprmé la requete  concern*/
                        $del = $draft->delete();
                        if ($del) {
                            return \response()->json(['code' => 500]);
                        }
                    }

                }
            } else {
                $fileExtension = $files->getClientOriginalExtension();
                if ($fileExtension === 'mp3') {
                    $fileName = 'Audio';
                } else {
                    $fileName = 'image';
                }
                $upload = Storage::disk('public')->put('uploads/apps', $files);
                $path = Storage::url($upload);

                /**
                 * si l'upload a marche alors la sauvegarder les infos dans la base de donneed
                 */
                if ($upload) {
                    $attach = $draft->attachments()->create([
                        'name' => $fileName,
                        'file_extension' => $fileExtension,
                        'attachment_url' => $path,
                        'is_attached'=>1
                    ]);
                    if ($attach) {
                        return \response()->json(['code' => 200]);
                    }
                } else {
                    /* vue l'export n.a pas marché supprmé la requete  concern*/
                    $del = $draft->delete();
                    if ($del) {
                        return \response()->json(['code' => 500]);
                    }
                }
            }
        }
    }

    public function noTreat(Request $request, Response $response)
    {
        $requests = Drafts::where('admin_id', null)->get();
        return $requests;
    }

    public function attach(Request $request, Response $response)
    {
        $draft = Drafts::findOrfail($request->draft_id);
        $draft->state = 1;
        $draft->admin_id = $request->traitor_id;
        $update = $draft->save();

        if ($update) {
            return \response()->json(['status' => 200]);
        } else {
            return \response()->json(['status' => 600]);
        }

    }

    public function sendForValidation(Request $request, Response $response)
    {
        $draft = Drafts::find($request->get('request_id'));
        $draft->state = 2;
        $update = $draft->to_validate = $request->get('to_id');
        $draft->save();
        if ($update) {
            $file_name = $request->file('pdfFile')->getClientOriginalName();
            $upload = Storage::disk('public')->put('uploads/apps', $request->file('pdfFile'));
            $path = Storage::url($upload);
            $final_file_name = str_replace(' ', '_', $file_name);
            $file_extension = $request->file('pdfFile')->getClientOriginalExtension();
            if ($path) {
                $attach = $draft->attachments()->create([
                    'name' => $final_file_name,
                    'attachment_url' => $path,
                    'file_extension' => $file_extension,
                    'validation' => 1
                ]);
                if ($attach) {
                    return \response()->json(['code' => 200, 'message' => 'La requête a bien été transmises']);
                } else {
                    $del = $draft->delete();
                    return \response()->json(['code' => 600, 'message' => 'Erreur lors de l\'ajout du fichier']);
                }
            }
        }
        // dd($request->all());
    }


    public function getInQueueRequest(Request $request, Response $response)
    {
        $requests = Drafts::select(
            'drafts.id',
            'drafts.state',
            'drafts.draft_subject',
            'attachment_url',
            'drafts.draft_quantity',
            'admins.username',
            'drafts.draft_content')
            ->leftjoin('admins', 'drafts.admin_id', 'admins.id')
            ->leftjoin('attachments', 'attachments.attachable_id', 'drafts.id')
            ->where('validation', '1')
            ->where('drafts.state', '=', 2)
            ->get();
        return $requests;
    }

    /*cancelRequest*/
    public function cancelRequest(Request $request, Response $response, $id)
    {
        $comments = $request->get('comments');
        if ($request->get('is_admin') == 1) {
            $annulateBy = User::select('users.id', 'user_type', 'admins.username')
                ->join('admins', 'admins.id', 'users.user_id')
                ->where('admins.id', '=', $this->authManager->guard()->user()->id)
                ->first();
            $cancel_process = DraftCancel::create([
                'draft_id' => $id,
                'cancel_by' => $annulateBy->id,
                'reason' => $comments,
                'is_admin' => 1
            ]);
            $cancel_draft = Drafts::findOrFail($id);
            $cancel_draft->state = -3;
            $update = $cancel_draft->save();

            if ($cancel_process && $update) {
                return \response()->json([
                    'update_status' => 200
                ]);
            } else {
                return \response()->json([
                    'update_status' => 500
                ]);
            }


        }

    }


    public function revalidateRequest(Request $request, Response $response, $id)
    {

        $requete = Drafts::findOrFail($id);

        $bill = $request->file('bill');
        $update_request = $requete->update([
            'state' => 2,
            'comment' => $request->get('comment')
        ]);
        /*delete the drafts in the requestCancelTable*/
        $del = DraftCancel::select('id', 'draft_id')->where("draft_id", $id)->delete();

        //rechercher l'ancienne facture et la desactiver en passant son validatio  a 0
        $attachemnt=Attachment::select('attachable_id','id','validation')
            ->where('attachable_id','=',$id)
            ->where('validation','=',1)
            ->first();
        $attachemnt->validation=0;
        $attachemnt->save();
        if ($del && $update_request) {
            $name = str_replace(' ', '_', $bill->getClientOriginalName());
            $upload = Storage::disk('public')->put('uploads/apps', $bill);
            $path = Storage::url($upload);
            $attach = $requete->attachments()->create([
                'name' => $name,
                'attachment_url' => $path,
                'file_extension' => $bill->getClientOriginalExtension(),
                'validation' => 1
            ]);

            if ($attach){
                return \response()->json([
                    'code'=>200
                ]);
            }
        }

    }

    public function validateBill(Request $request, Response $response)
    {
        if ($request->hasFile('finishBill')) {
            $file = $request->file('finishBill');
            $id = $request->get('idRequest');
            $requete = Drafts::findOrFail($id);
            $update_request = $requete->update([
                'state' => 3
            ]);
            $insert=DraftValidate::create([
                'draft_id'=>$request->get('idRequest'),
                'approved_by'=>$this->authManager->guard()->user()->id
            ]);

            if ($update_request && $insert) {
                $originalName = $file->getClientOriginalName();
                $originalExtension = $file->getClientOriginalExtension();
                $requete = Drafts::findOrFail($id);
                $path = Storage::disk('public')->put('webApp/documents', $file);
                $url=Storage::url($path);
                $attach = $requete->attachments()->create([
                    'name' => $originalName,
                    'file_extension' => $originalExtension,
                    'attachment_url' => $url,
                    'validation' => 3 //facture validé par les adminstrateurs
                ]);
                if ($attach) {
                    return \response()->json(['code' => 200]);
                } else {
                    return \response()->json(['code' => 404]);
                }
            } else {
                return \response()->json(['code' => 404]);
            }
        } else {
            return \response()->json(['code' => 404]);
        }

    }

    public function  getValidateRequest(Request $request,Response $response)
    {
        return $this->draftsRepository->getValidatedRequests();
    }


    public function showSingleRequest(Request $request, Response $response, int $id)
    {

        $draft = $this->draftsRepository->showSingleRequest($id);
        $attachments = $this->draftsRepository->getAttachments($id);
        $paymentsProcess = $this->draftsRepository->getDraftsPaymentProcess($id);
        return \response()->json([
            'draft' => $draft,
            'attachments'=>$attachments,
            'payments'=>$paymentsProcess
        ]);

    }


    public function setReadable(Request $request,Response $response){

        $draft=Drafts::find($request->get('id'));
        $draft->state=9;
        $draft->save();
    }


    public function respondeToUser(Request $request,Response $response,$id){
        $client=\App\Client::find($id);
        $draft_id=$request->get('request_id');
        $_request=Drafts::find($draft_id);
        $_request->update([
            'state' => 4
        ]);
        FireBaseHelpers::sendNotification($request->get('request_id'),$client->fcm_token);
        return \response()->json([
            'code'=>200
        ]);

    }
}
