<?php


namespace App\Http\Controllers;

use App\Admin;
use App\Attachment;

use App\Http\Repository\AdminRepository;
use App\Slide;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;

class ApiAdminController extends Controller
{

    private $adminRepository;

    public function __construct(AdminRepository $adminRepository)
    {
        $this->adminRepository=$adminRepository;
    }

    public function logUser(Request $request, Response $response)
    {

        $username = $request->get("username");
        $password = $request->get("password");


        $admin = Admin::select('id', 'username', 'password', 'role_id')
            ->where('username', '=', $username)
            ->where('password','=',Hash::check('password',$password))
            ->first();

        if (!$admin) {
            return \response()->json(
                [
                    'status_code' => 404,
                    'admin' => null
                ]
            );
        } else {
            return \response()->json(
                [
                    'status_code' => 200,
                    'admin' => $admin
                ]);
        }
    }

    public function getLastRequest(Request $request,Response $response){
        return $this->adminRepository->getRequests();
    }

    public function getMyRequestList(Request $request,Response $response,$id){
        return $this->adminRepository->getAllMyAssignedRequestByApi($id);
    }

    public function getTreatList(Request $request,Response $response){
        return $this->adminRepository->getAllTreatRequest();
    }

    public function getSingleDetails($id){

        $draft=$this->adminRepository->getApiSingleRequestDetails($id);
        if ($draft==null){
            echo "dds";
        }else{
            return $draft;
        }
    }

    public function updateCaroussel(Request $request,Response $response){

        $files=Storage::disk('public')->files('uploads/carousel');

        $del=Storage::delete($files);
        $delDb=Slide::truncate();
        $slides=Slide::all();


        if ($slides->isEmpty()){

            $files=$request->file('image');
            $isUploaded=false;
            foreach ($files as $file){

                $upload = Storage::disk('public')->put('uploads/carousel', $file);
                $path = Storage::url($upload);

                $insertion= Slide::create([
                    'image_url'=>$path
                ]);

                if ($insertion){
                    $isUploaded=true;
                }else{
                    $isUploaded=false;
                }

            }

            if ($isUploaded){
                return \response()->json([
                    'code'=>200
                ]);
            }else{
                return \response()->json([
                    'code'=>400
                ]);
            }
        }else{
            return \response()->json([
                'code'=>500
            ]);
        }

    }

    public function loadSlides(Request $request,Response $response){
        return Slide::all();
    }
}
