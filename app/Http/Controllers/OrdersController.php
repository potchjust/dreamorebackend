<?php

namespace App\Http\Controllers;


use App\Client;
use App\Drafts;
use App\Http\Helpers\FireBaseHelpers;
use App\Http\Repository\OrderRepository;
use App\Http\Repository\RequestRepository;
use App\Payment;
use App\Purchase;
use App\SubPayment;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrdersController extends Controller
{
    //

    /**
     * @var AuthManager
     */
    private $authManager;

    /**
     * @var OrderRepository
     */
    private $_orderRepo;
    /**
     * @var RequestRepository
     */
    private $requestRepo;


    public function __construct(AuthManager $authManager)
    {
        $this->authManager = $authManager;
        $this->_orderRepo = new OrderRepository($authManager);
        $this->requestRepo = new RequestRepository($authManager);
    }

    public function prepareCommand(Request $request,Response $response,$id){
        $request=Drafts::find($id);
        $request->state=4;
        $update=$request->save();
        if ($update){
            return \response()->json(['statut'=>200]);
        }else{
            return \response()->json(['statut'=>500]);
        }
    }
    public function EmmitPurchase(Request $request,Response $response,$id)
    {
        $requete=Drafts::findOrFail($id);
        $user=Client::find($requete->client_id);


        if ($requete){
            $commande=Purchase::create([
                'draft_id'=>$id,
                'total_price'=>$request->get('total'),
            ]);
            if ($commande){
                /*modifié l'etat de sa requete en passant en le statut à 5*/

                $requete->state=6;
                $requete->total=$request->get('total');
                $requete->save();
                FireBaseHelpers::sendNotification($requete->id,$user->fcm_token,"Votre demande a bien été validé vous pouvez donc proceder au paiement de votre commande");

                return \response()->json([
                    'emmitOrderStatus'=>200
                ]);
            }
        }
        else{
            return \response()->json([
                'emmitOrderStatus'=>500
            ]);
        }
    }



    public function getOrders(Request $request,Response $response){
        $preorders=Client::select('clients.fname','clients.lname','clients.id')
            ->join('drafts','drafts.client_id','clients.id')
            ->where('drafts.state','=',6)
            ->groupBy('clients.id')
            ->get();
        return $preorders;
    }

    /*
     * */

    public function getOrderDetails(Request $request,Response $response,$id){
        $preorders=Purchase::select('drafts.client_id','purchases.id','purchases.draft_id',
            'purchases.total_price','drafts.admin_id','purchase_state',
            'drafts.draft_subject','clients.fname','drafts.state','clients.lname','drafts.draft_quantity','drafts.draft_deliver_mode')
            ->leftjoin('drafts','drafts.id','purchases.draft_id')
            ->leftjoin('clients','drafts.client_id','clients.id')
            ->where('drafts.state','=',8)
            ->where('drafts.admin_id',$this->authManager->guard()->user()->id)
            ->where('clients.id','=',$id)
            ->get();
        return $preorders;
    }

    public function sendBillToUser(Request $request,Response $response){
        $order=$request->get('order');
        $client=Client::find($order['client_id']);
        FireBaseHelpers::sendNotification($order['draft_id'],$client->fcm_token,$order);
        return \response()->json(['statut'=>200]);
    }

    public function initPayment(Request $request,Response $response){
        $payment=Purchase::create([
            'total_price'=>$request->get('total'),
            'draft_id'=>$request->get('draft_id'),
            "purchase_state"=>1
        ]);
        if ($payment){
            return $payment;
        }
    }

    public function proceedToPay(Request $request,Response $response,$request_id){


        $purchase = Purchase::select("purchases.purchase_state",'purchases.total_price',
            'purchases.id as id','purchases.draft_id','drafts.state as drafts_state')
            ->join('drafts','drafts.id','purchases.draft_id')
            ->where('purchases.id',$request_id)
            ->first();




        $draft=Drafts::find($purchase->draft_id);
        $draft->state=7;
        $draft->save();


        //update the purchase
        $purchase->purchase_state=1;
        $purchase->save();


        $sub=SubPayment::create([
            'paid'=>$request->get("paid"),
            'purchase_id'=>$purchase->id,
            "rest"=>$request->get("total")-$request->get("paid"),
            "paid_at"=>Carbon::now(),
        ]);

        return $sub->id;

    }

    public function checkExistingPaymentProcess(Request $request,Response $response,$id){
        $pay=Payment::where('draft_id','=',$id)
            ->where('state',"=",'1')
            ->first();

        if ($pay!=null){
            return $pay;
        }else{
            return \response()->json([
                "id"=>0,
                "total"=>0,
                "draft_id"=>0,
                "state"=>0
            ]);
        }
    }

    public function checkIfPaid(Request $request,Response $response,$id){
        $pay=Payment::where('purchase_id','=',$id)
            ->where('state',"=",'0')
            ->first();
        //si pay=null donc il n;y pas de processus de paiement donc le bouton peut s'afficher
        if ($pay==null){
            return null;
        }else{
            return $pay;
        }
    }


    /**
     * @param Request $request
     * @param Response $response
     * @param $id is the purchases number
     * @return mixed
     */
    public function getSubpayments(Request $request, Response $response, $id)
    {

        $sub=SubPayment::where('purchase_id','=',$id)
            ->get();
        return $sub;
    }


    public function updateStatus(Request $request,Response $response,$sub_id){


        $subpayment=SubPayment::find($sub_id);
        $subpayment->identifier=$request->get('identifier');
        $subpayment->payment_method=$request->get('paymentMethod');
        $subpayment->status=$request->get('paymentStatus');
        $subpayment->save();

        /*change the purchase state to two  */
        $purchase=Purchase::where('id',$subpayment->purchase_id)
                            ->update([
                               'purchases.purchase_state' =>2
                            ]);

        if ($purchase ==1){
            return $request->get('paymentStatus');
        }else{
            return 300;
        }

    }



    public function listPreorderedRequest(Request $request){
      return $this->_orderRepo->loadPreorderInqueueRequest();
    }


    public function getAllPaymentProcess(Request $request){
        return $this->_orderRepo->getAllPaymentProcess();
    }

}
