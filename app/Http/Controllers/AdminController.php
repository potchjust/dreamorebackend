<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Client;
use App\Drafts;
use App\Fournisseur;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Helpers\FireBaseHelpers;
use App\Http\Helpers\Utils;
use App\Http\Repository\AdminRepository;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel as ExcelAlias;

class AdminController extends Controller
{
    private $admin_repository;

    public function __construct(AdminRepository $admin_repository)
    {
        $this->middleware('auth');
        $this->admin_repository = $admin_repository;
    }

    public function getAdmins()
    {
        $admins = $this->admin_repository->getOtherAdmins();
        return \response()->json($admins);

    }

    public function getTraitors()
    {
        $admins = Admin::where('role_id', 2)->get();
        return $admins;
    }

    public function getMyLastTenAssignedRequest(Request $request, Response $response)
    {
        return $this->admin_repository->getMyLastTenAssignedRequest();
    }

    public function getAllMyAssignedRequest(Request $request, Response $response)
    {
        return $this->admin_repository->getAllMyAssignedRequest();
    }

    public function getMyCancelRequestList(Request $request, Response $response)
    {
        return $this->admin_repository->getMyCancelRequestList();
    }

    public function getRequestByClient(Request $request, Response $response, $id)
    {
        return $this->admin_repository->getRequestByClient($id);
    }

    public function getAllClient(Request $request, Response $response)
    {
        return Client::all();
    }

    public function notifyByPush(Request $request, Response $response, $token)
    {
        FireBaseHelpers::sendSingleNotification($token, $request->get("message"));
    }

    public function getOrderedUsers(Request $request, Response $response)
    {
        $query=$request->input('client');
        return $this->admin_repository->getOrderedClients($query);
    }

    public function notifyBySms(Request $request, Response $response, $user)
    {
        $content = $request->get('message');
        Utils::sendUniqueSms($user, $content);
    }

    public function importFile(Request $request, Response $response)
    {
        ExcelAlias::import(new Fournisseur, $request->file('list'));
        return back();
    }

    public function getFournisseurList(Request $request,Response $response){
        return Fournisseur::all();
    }

    public function deleteFournisseur(Request $request,Response $response,$id){
        $fournisseur=Fournisseur::find($id);
        $fournisseur->delete();
        $fournisseur=Fournisseur::find($id);
        if ($fournisseur==null){
            return \response()->json(['code'=>200]);
        }
    }

    public function getTreatRequestList(Request $request,Response $response){
        $treatList=Drafts::select('drafts.id','drafts.draft_subject','clients.fname','clients.lname','admins.username')
            ->join('clients','clients.id','drafts.client_id')
            ->join('admins','admins.id','drafts.admin_id')
            ->get();
        $treatListCount=count($treatList);
        $notreatListCount=count($this->noTreatRequestList($request,$response));
        return \response()->json(
            [
             'treatList'=>$treatList,
             'treatListCount'=>$treatListCount,
             'notreatListCount'=>$notreatListCount,
            ]
        );

    }

    public function noTreatRequestList(Request $request,Response $response){
        return Drafts::select('drafts.id','drafts.draft_subject','clients.fname','clients.lname','admins.username')
            ->join('clients','clients.id','drafts.client_id')
            ->leftjoin('admins','admins.id','drafts.admin_id')
            ->where('drafts.admin_id','=',null)
            ->get();
    }


    public function logUser(Request $request,Response $response){

        $username=$request->get("username");
        $password=$request->get("password");


        $admin=Admin::select('id','username','password','role_id')
            ->where('username','=',$username)
            ->where('password','=',$password)
            ->first();



        if (!$admin){
            return \response()->json(
                [
                    'status_code'=>404,
                    'admin'=>null
                ]
            );
        }else{
            return \response()->json(
                [
                    'status_code'=>404,
                    'admin'=>$admin
                ]);
        }
    }




}
