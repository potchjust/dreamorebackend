<?php

namespace App\Http\Controllers;

use App\DraftCancel;
use App\DraftValidate;
use App\Http\Repository\RequestRepository;
use App\User;
use Kreait\Firebase;
use Kreait\Firebase\Messaging\CloudMessage;
use App\Attachment;
use App\Client;
use App\Drafts;
use App\Events\SendNotificationToUser;
use App\Events\ShowRequestReceivedNotification;
use App\Http\Helpers\FireBaseHelpers;
use App\Http\Requests\RequetesRequest;
use App\RequestReturn;
use App\Requete;
use Carbon\Carbon;
use Faker\Provider\Lorem;
use Illuminate\Http\Response as Reponse;
use Illuminate\Http\Request as Request;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use Kreait\Firebase\Messaging\Notification;
use Symfony\Component\HttpFoundation\File\Exception\FileException;


class RequetesController extends Controller
{
    //

    /**
     * @var RequestRepository
     */
    private $repository;



    public function __construct(RequestRepository $repository)
    {
        $this->repository = $repository;
        //$this->middleware('auth');
    }


    public function getRequestList()
    {
        $drafts_response = $this->repository->showAllRequests();
        return \response()->json(['drafts' => $drafts_response]);
    }


    /**
     * @param Request $request
     * @param Response $response
     * @param Drafts $draft
     */
    public function show(Request $request, Response $response)
    {
        return \view('requetes.single');
        //  $draft = $this->repository->showSingleRequest($drafts->id);
    }

    public function showSingleRequest(Request $request, Response $response, int $id)
    {
        $attachments = $this->repository->getAttachments($id);
        $is_request = false;

        $_request = Requete::join('drafts', 'requetes.draft_id', 'drafts.id')->where('requetes.draft_id', '=', $id)->first();
        $draft = $this->repository->showSingleRequest($id);
        return \response()->json(['attachment' => $attachments, 'draft' => $draft]);

    }

    /**
     * update the request and attach it to an admin
     * @param Request $request
     * @param Response $response
     * @return
     */


    public function initRequest(Request $request, Response $response)
    {

        /**
         * Insert la requête et après je fais
         */
        $uploaded_file = $request->file('pdfFile');
        $file_name = $uploaded_file->getClientOriginalName();
        $final_file_name = str_replace(' ', '_', $file_name);
        $file_extension = $uploaded_file->getClientOriginalExtension();

        $update_requete = Requete::where('draft_id', '=', $request->get('request_id'))->update([
            'statut' => 0,
            'from_id' => $request->get('from_id'),
            'to_id' => $request->get('to_id'),
        ]);

        $requete = Requete::where('draft_id', '=', $request->get('request_id'))->first();

        if ($update_requete) {
            /**
             *si la requête a été inseré alors on essaie l'insertion des fichiers attaché
             *si l'insertion ne marche pas je supprîme la requete
             **/
            $request_id = $requete->id;
            try {
                $uploading = Storage::disk('public')->put('webApp/documents', $uploaded_file);
                $url = Storage::url($uploading);
                if ($uploading) {
                    $attach = $requete->attachments()->create([
                        'name' => $final_file_name,
                        'attachment_url' => $url,
                        'file_extension' => $file_extension
                    ]);
                    if ($attach) {
                        return \response()->json(['code' => 200, 'message' => 'La requête a bien été transmise']);
                    } else {
                        $del = $requete->delete();
                        return \response()->json(['code' => 600, 'message' => 'Erreur lors de l\'ajout du fichier']);
                    }
                }
            } catch (FileException $exception) {
                $del = $requete->delete();
                return \response()->json(['code' => 600, 'message' => 'Erreur lors de l\'upload  du fichier la requête a été supprimé']);
            }
        } else {
            return \response()->json(['code' => 700, 'message' => 'Erreur lors de l\'insertion  du fichier la requête a été supprimé']);
        }
    }

    /*
     * pour les requetes en attentes de validation celui qui les a envoy ne pourra pas
     * voir les autres buttons
     * */
    public function getInQueueRequest(Request $request, Response $response)
    {
        $requests = $this->getRequestWithAttachments();
        return \response()->json(['requests' => $requests]);
    }

    public function getRequestWithAttachments()
    {
        return $this->repository->getRequestWithAttachments();
    }

    /*
     * l'administrateur a validé par l'admin et donc son statut passe à un
     * */


    /*
     * requete de validation
     */


    public function getSingleValidateDetails(Request $request, Response $response, $id)
    {
        return $this->repository->getSingleDetails($id);
    }


    public function getAllCancelRequestList(Request $request, Response $response)
    {
        return $this->repository->getRejectedRequests();
    }

    public function revalidateRequest(Request $request, Response $response, $id)
    {

        $requete = Requete::findOrFail($id);
        $update_request = $requete->update([
            'statut' => 0,
            'comment' => $request->get('comment')
        ]);
        if ($update_request) {
            /*on va modifier aussi le fichier attaché*/
            $attachment = Attachment::where('attachments.attachable_type', '=', 'App\Requete')
                ->where('attachable_id', $id)
                ->first();
            $del_file = Storage::disk('public')->delete($attachment->path);
            if ($del_file) {
                $path = Storage::disk('public')->put('webapp', $request->file('bill'));
                $full_url = Storage::url($path);
                $update_attachement = $attachment->update([
                    'name' => $request->file('bill')->getClientOriginalName(),
                    'file_extension' => $request->file('bill')->getClientOriginalExtension(),
                    'attachment_url' => $full_url,
                    'path' => $path
                ]);
                if ($update_attachement) {
                    return \response()->json([
                        'revalidate_status' => 200
                    ]);
                }
            } else {
                echo 'imposiible suppr';
            }
        }

    }


    public function acceptRequest(Request $request, Response $response)
    {


        $client_id = $request->get('client_id');
        $request_id = $request->get('request_id');
        $content = $request->get('content');
        $attachment_url = $request->get('requestBillUrl');

        /*rechercher  et modifier l'id pour aue la requete passe a deux*/
        $requete = Requete::findOrFail($request_id);
        $requete->statut = 2;
        $requete->comment = $content;
        $updateRequest = $requete->save();
        if ($updateRequest) {
            if ($updateRequest) {
                $attach = RequestReturn::create([
                    'request_id' => $request_id,
                    'client_id' => $client_id,
                    'attachment_url' => $attachment_url
                ]);
                if ($attach) {
                    /*on envoie l'id vers lal'activity single
                     et la ce serait plus facile a gerer
                    et sur cette activity on va afficher un bouton au moment ou dans le bundle on detecte
                    un tag response et lorsqu'on clique dessus on va lancé le telechargemet du fichier et l'ouvrir
                    dans la PDFView
                    Voici mon analyse
                    */
                    $client = Client::find($client_id);
                    FireBaseHelpers::sendNotification($request_id, $requete->statut, $client->fcm_token, "dd");
                    return $attach;
                }

            }

        }
    }

    public function getNoTreat(Request $request, Response $response)
    {
        $requests = Drafts::where('admin_id', 0)->get();
        return $requests;
    }

    public function getAcceptance(Request $request, Response $response, $id)
    {
        $info = RequestReturn::where('request_id', $id)->first();
        return $info;
    }


    /*pour valider la requetes on aura besoin du draft_id*/


}
