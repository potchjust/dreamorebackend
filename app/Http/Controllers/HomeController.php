<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __construct()
    {

    }

    public function index()
    {
        return view('layouts.app');
    }

    public function showtest()
    {
        return view('testView');
    }
    public function getAuthUser(Request $request)
    {
        return response()->json($request->user()->id);
    }
}
