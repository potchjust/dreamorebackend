<?php
//TODO changer les code de retour
namespace App\Http\Controllers;


use App\Attachment;
use App\Client;
use App\Discussion;
use App\DraftCancel;
use App\Drafts;
use App\DraftValidate;
use App\Requete;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response as Response;

class ClientController extends Controller
{
    //

    private $ski = "dsd";
    public function construct()
    {
        $this->middleware("customer_api");
    }

    public function checkExistingUser(Request $request, Response $response, $telephone)
    {
        $data=[
            "clients.id as id",
            "clients.lname",
            "clients.fname",
            "clients.address",
            "clients.email",
            "clients.telephone",
            "clients.fcm_token",
            "clients.country_code",
            "users.user_type",
            "users.id as userId"];

        $client = Client::where('telephone', $telephone)
            ->join('users','users.user_id','clients.id')
            ->where('user_type',Client::class)
            ->first($data);
        if ($client) {
            return \response()->json($client);
        } else {
            return \response()->json(['code' => '404']);
        }
    }

    public function register(Request $request)
    {
        /**
         * devra se charger d'inscrire l'utilisateur et retourné un statut
         */
        /**
         * d'abord on verife si personne n'a le même numéro dans le systeme
         * si il existe alors on le loge directement afin qui'il retrouve ses information
         */
        $client_insert = Client::create([
            'lname' => $request->get('client_lname'),
            'fname' => $request->get('client_fname'),
            'email' => $request->get('client_email'),
            'telephone' => $request->get('client_tel'),
            'country_code'=>$request->get('country_code')
        ]);
        if ($client_insert) {
            $user = $client_insert->user()->create();
            if ($user) {
                return \response()->json([
                    'code' => 200,
                    'client_id' => $client_insert->id
                ]);
            }

        }

    }

    /**
     * recupère toute les requêtes effectué par l'user
     * @param Request $request
     * @param Response $response
     * @param $id
     */

    public function getRequest(Request $request, Response $response, $id)
    {
        $drafts = Drafts::select('drafts.id','requetes.statut' ,'draft_subject', 'received_at', 'admin_id')->where('client_id', '=', $id)
            ->orderBy('received_at', 'DESC')
            ->join('requetes','requetes.draft_id','drafts.id')
            ->get();
        return $drafts;
    }
    public function getMyLastRequest(Request $request, Response $response, $id)
    {
        $drafts = Drafts::select('drafts.id','drafts.state' ,'draft_subject', 'received_at', 'admin_id')->where('client_id', '=', $id)
            ->orderBy('received_at', 'DESC')
            ->get();
        return $drafts;
    }

    public function getRequestDetails(Request $request, Response $response,  $client_id,$draft_id)
    {


        $data=[
            'drafts.id as id',
            'drafts.draft_subject',
            'drafts.draft_quantity',
            'drafts.draft_content',
            'drafts.draft_deliver_mode',
            'drafts.client_id',
            'drafts.admin_id',
            'drafts.received_at',
            'drafts.state',
            'purchases.purchase_state',
            'purchases.total_price',
            'purchases.id as purchase_id',
            'attachments.validation',
            'attachment_url',
            'users.id as adminUser_id',
            'admins.username'
        ];

        $draft=Drafts::leftJoin('attachments',function ($join){
            $join->on('attachments.attachable_id','drafts.id');
            $join->where('attachments.validation',3);
        })
            ->leftjoin('purchases','purchases.draft_id','drafts.id')
            ->where('drafts.client_id',$client_id)
            ->where('drafts.id',$draft_id)
            ->leftjoin('users','users.user_id','drafts.admin_id')
            ->leftjoin('admins','admins.id','drafts.admin_id')
            ->first($data);
        return \response()->json(
            ['draft'=>$draft]
        );

    }




    public function getClientValidateRequest(Request $request,Response $response)
    {

        return Drafts::select('clients.id',"clients.fname",'clients.lname','requetes.statut','drafts.draft_subject')
            ->join('clients','clients.id','drafts.client_id')
            ->join('requetes','requetes.draft_id','drafts.id')
            ->where('requetes.statut','=',3)
            ->groupBy('clients.id')
            ->get();
    }

    public function saveToken(Request $request,Response $response,$id){
        $client=Client::find($id);
        $add_token=$client->fcm_token=$request->get('fcm_token');
        $client->save();
    }

    public function updateToken(Request $request,Response $response,$id){
        $client=Client::find($id);
        $client->fcm_token=$request->get('token');
        $client->save();
    }

    public function sayOk(Request $request, Response $response, $id)
    {

        $draft=Drafts::find($id);
        $validate = Drafts::where('id', $id)
            ->update([
                'state' => 5,
            ]);
        $user = User::select('users.id', 'users.user_id','clients.fname')
            ->join('clients', 'clients.id', 'users.user_id')
            ->where('users.user_id', '=', $draft->client_id)
            ->first();
        $validation = DraftValidate::create([
            'draft_id' => $id,
            'approved_by' => $user->id
        ]);
        if ($validation->id>0) {
            return \response()->json($draft);
        } else {
            return \response()->json(['code' => 600]);
        }

    }


    public function sayNo(Request $request, Response $response, $id)
    {

        $draft=Drafts::find($id);
        $update = Drafts::where('id', $id)
            ->update([
                'state' => -5,
            ]);
        $user = User::select('users.id', 'users.user_id')
            ->join('clients', 'clients.id', 'users.user_id')
            ->where('users.user_id', '=', $draft->client_id)
            ->first();
        $cancels=DraftCancel::create([
            'draft_id'=>$draft->id,
            'reason' => $request->get('comment'),
            'cancel_by'=>$user->id
        ]);

        if ($cancels->id>0) {
            return \response()->json($draft);
        } else {
            return \response()->json(['code' => 600]);
        }


    }

}
