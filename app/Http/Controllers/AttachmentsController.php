<?php

namespace App\Http\Controllers;

use App\Attachment;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class AttachmentsController extends Controller
{
    //
    public function getAttachedFile(Request $request, Response $response, $id)
    {
        $file = Attachment::join('requetes', 'attachable_id', 'requetes.id')->where('attachable_id', '=', $id)->first();
        return \response()->json($file);
    }
}
