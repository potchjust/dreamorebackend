<?php

namespace App\Http\Controllers;

use App\Client;
use App\Discussion;
use App\Drafts;
use App\Events\UserSendRequest;
use App\Requete;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Kreait\Firebase\Auth;
use Illuminate\Redis;

class DiscussionsController extends Controller
{
    //

    /**
     * @var AuthManager
     */
    private $authManager;

    public function __construct(AuthManager $authManager)
    {
        $this->authManager = $authManager;
    }

    public function initDiscussions(Request $request, Response $response)
    {
        $client_id = $request->get('client_id');
        $request_id = $request->get('request_id');
        $content = $request->get('content');
        $attachment_url = $request->get('requestBillUrl');

        $init_discussions = Discussion::create(
            [
                'content' => $content,
                'draft_id' => $request_id,
                'to_id' => $client_id,
                'from_id' => 1,
                'discuss_at' => Carbon::now()
            ]
        );
        if ($init_discussions) {
            $discussion = Discussion::findOrFail($init_discussions->id);
            $requete = Requete::findOrFail($init_discussions->request_id);
            $requete->statut = 2;
            $updateRequest = $requete->save();
            if ($updateRequest) {
                return \response()->json(['code' => 200]);
            } else {
                $del = $discussion->delete($init_discussions->id);
                return \response()->json(['code' => 400]);
            }
        } else {
            return \response()->json(['code' => 500]);
        }

    }

    public function getDiscussions(Request $request, Response $response, $customer_id, $request_id)
    {
        $discussion = Discussion::select('discussions.id', 'discussions.content', 'discuss_at', 'discussions.request_id', 'discussions.from')
            ->where('discussions.request_id', "=", $request_id)
            ->where('discussions.client_id', "=", $customer_id)
            ->get();

        return $discussion;
    }


    /*
     *recuperer  les utilisateurs aui ont envoyer un message
     */
    public function loadSingleChatDetails(Request $request,Response $response,$draft_id){

        return Discussion::select()
            ->join('drafts','drafts.id','discussions.draft_id')
            ->where(function ($query){
                $adminUser = User::where('user_id',$this->authManager->guard()->user()->id)->first();
                $query->where('from_id','=',$adminUser->id)
                    ->orWhere('to_id','=',$adminUser->id);
            })->where('draft_id','=',$draft_id)->get();

    }
    /*
     * fonction pernettant de repondre a un utilisateur
     */

    public function adminRespondeToUser(Request $request,Response $response){
           $discussions= Discussion::create([
                'content'=>$request->get('content'),
                'from'=>1,
                'admin_id'=>$this->authManager->guard()->user()->id,
                'request_id'=>$request->get('request_id'),
                'client_id'=>$request->get('client_id')
            ]);

    }
    public function getAllDiscussions(Request $request,Response $response,$client_id){

        $discussions=Discussion::select('request_id','from','client_id','content')
            ->where('client_id',$client_id)
            ->groupBy('request_id')
            ->get();
        return $discussions;
    }


    public function adminSendUser(Request $request,Response $response){

    }

    public function adminRetreiveAllMessages(Request $request,Response $response){

       return Discussion::select('drafts.id','drafts.draft_subject')
            ->join('drafts','drafts.id','discussions.draft_id')
             ->where(function ($query){
                 $adminUser = User::where('user_id',$this->authManager->guard()->user()->id)->first();
                 $query->where( "discussions.to_id",'=',$adminUser->id)
                     ->orWhere( "discussions.from_id",'=',$adminUser->id);
             })

            ->groupBy('drafts.id')
            ->get();
    }


    /*
     * fonction permettant de repondre aux que ce soit le client ou à l'admin de repondre
     * */

    public function responde(Request $request,Response $response){

        $adminUser = User::where('user_id',$this->authManager->guard()->user()->id)->first();
        $clientUser = Userwhere('user_id',$request->get('client_id'));

        $body = [];

        $body[0]= $request->get("content");
        $body[1]= $request->get("draft_id");

        if ($request->get('isAdmin')){
          $body[2]=$adminUser->id;
          $body[3]=$clientUser->id;
           $this->createDiscussion($body);

        }else{
            $body[2]=$clientUser->id;
            $body[3]=$adminUser->id;
            $this->createDiscussion($body);
        }

    }

    public function createDiscussion(...$params){
        $createDiscussion =  Discussion::create([
                "content"=>$params[0],
                "discuss_at"=>Carbon::now(),
                "draft_id"=>$params[1],
                "from_id"=>$params[2],
                "to_id"=>$params[3],
        ]);
        if ($createDiscussion){
            return \response()->json([
                'code'=>200,
                "message"=>"Message envoyé avec succès"
            ]);
          }else{
            return \response()->json([
                'code'=>600,
                "message"=>"Erreur lors de l'envoi du message"
            ]);
        }

    }


}
