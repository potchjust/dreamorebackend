<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DraftValidate extends Model
{
    //

    protected $fillable=["bill_url",'approved_by','draft_id'];
    public $timestamps=false;

    public function draft(){
        return $this->belongsTo(Drafts::class);
    }

    public function approved(){
        return $this->belongsTo(User::class);
    }
}
