<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{

    protected $fillable =['name','file_extension','validation','attachable_type','attachable_id','attachment_url','is_bill','is_attached'];

    public function attachable()
    {
        return $this->morphTo();
    }

    public $timestamps = false;
}
