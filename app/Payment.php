<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $fillable =['total','purchase_id','state'];
    public $timestamps=false;

    public function subpayments(){
        return $this->hasMany(SubPayment::class);
    }

    public function purchase(){
        return $this->hasOne(Purchase::class);
    }



}
