<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_type',
        'user_id',

    ];

    public function user()
    {
        return $this->morphTo();
    }

    public function approved(){
        return $this->hasMany(DraftValidate::class);
    }





    public function cancel(){
        return $this->$this->hasMany(DraftCancel::class);
    }

    public $timestamps = false;
}
