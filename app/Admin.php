<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable;


    protected $fillable = ['username', 'password', 'role_id'];
    public $timestamps = false;

    //



    public function role()
    {
        return $this->belongsTo(Role::class);
    }


    public function drafts()
    {
        return $this->hasMany(Drafts::class);
    }

    public function user()
    {
        return $this->morphMany(User::class, 'user');
    }

    public function to_id()
    {
        return $this->hasMany(Requete::class, 'to_id');
    }

    public function from_id()
    {
        return $this->hasMany(Requete::class, 'from_id');
    }

    public function discussions(){
        return $this->hasMany(Discussion::class);
    }

    public function validate(){
        return $this->hasMany(Drafts::class);
    }



}
