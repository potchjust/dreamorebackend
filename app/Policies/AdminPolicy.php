<?php

namespace App\Policies;

use App\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */

    public function before(Admin $admin)
    {
        if ($admin->role_id==1){
            return true;
        }
    }
    public function __construct(Admin $admin)
    {
        //
        if ($admin->role_id==1){
            return true;
        }
    }

    public function viewLink(Admin $admin,$ability){
        return $admin->id;
    }
}
