<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestReturn extends Model
{
    //

    protected $table='requests_returns';
    protected $fillable=['request_id','client_id','attachment_url'];
    public $timestamps=false;

}
