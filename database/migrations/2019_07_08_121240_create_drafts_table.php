<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDraftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('drafts')){
            Schema::create('drafts', function (Blueprint $table) {
                $table->increments('id');
                $table->string('draft_subject');
                $table->integer('draft_quantity');
                $table->text('draft_content');
                $table->integer('draft_deliver_mode');
                $table->integer('client_id')->unsigned()->nullable();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->foreign('client_id')->references('id')->on('clients');
                $table->foreign('admin_id')->references('id')->on('admins');
                $table->dateTime('received_at');//date où le client l'a envoyé
                $table->dateTime('process_at');//date où elle a commence par être traiter
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drafts', function (Blueprint $blueprint) {
            $blueprint->dropForeign('drafts_client_id_foreign');
            $blueprint->dropForeign('drafts_admin_id_foreign');

            $blueprint->dropColumn('client_id');
            $blueprint->dropColumn('admin_id');
        });
        Schema::dropIfExists('drafts');
    }
}
