<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFromToDiscussions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

            Schema::table('discussions', function (Blueprint $table) {
                if (!Schema::hasColumn("discussions","from")){
                    $table->integer('from');
                }

            });
        }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('to_discussions', function (Blueprint $table) {
            //
        });
    }
}
