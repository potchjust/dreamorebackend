<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("purchases")){
            Schema::create('purchases', function (Blueprint $table) {
                $table->increments('id');
                $table->string('receipt_number');
                $table->string('bank_receipt_number');
                $table->integer('total_price');
                $table->boolean('is_advance');
                $table->integer('advance_price')->nullable();
                $table->date('purchase_at');
                $table->integer('requete_id')->unsigned()->nullable();
                $table->foreign('requete_id')->references('id')->on('requetes');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchases', function (Blueprint $blueprint) {
            $blueprint->dropForeign('purchases_requete_id');
            $blueprint->dropColumn('requete_id');
        });
        Schema::dropIfExists('purchases');
    }
}
