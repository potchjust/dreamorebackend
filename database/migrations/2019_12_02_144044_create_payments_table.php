<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('payments')){
            Schema::create('payments', function (Blueprint $table) {
                $table->increments('id');
                $table->integer("total");
                $table->integer("draft_id")->nullable()->unsigned();
                $table->foreign("draft_id")->references("id")->on('drafts');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
