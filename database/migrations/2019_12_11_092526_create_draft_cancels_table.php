<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDraftCancelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('draft_cancels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("draft_id")->unsigned()->nullable();
            $table->string("bill_url");
            $table->foreign("draft_id")->references('id')->on('drafts');
            $table->integer("cancel_by")->nullable()->unsigned();
            $table->foreign("cancel_by")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('draft_cancels');
    }
}
