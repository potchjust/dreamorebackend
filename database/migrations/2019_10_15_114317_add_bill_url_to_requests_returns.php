<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillUrlToRequestsReturns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     if (!Schema::hasColumn("requests_returns",'attachment_url')){
         Schema::table('requests_returns', function (Blueprint $table) {
             $table->string('attachment_url');
         });
     }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requests_returns', function (Blueprint $table) {
            //
        });
    }
}
