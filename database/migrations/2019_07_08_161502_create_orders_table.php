<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('orders')){
            Schema::create('orders', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('purchase_id')->unsigned()->nullable();
                $table->foreign('purchase_id')->references('id')->on('purchases');
                $table->dateTime('ship_at');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $blueprint) {
            $blueprint->dropForeign('orders_purchase_id_foreign');
            $blueprint->dropColumn('purchase_id');
        });
        Schema::dropIfExists('orders');
    }
}
