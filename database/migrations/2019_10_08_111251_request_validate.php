<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RequestValidate extends Migration
{

    public function up()
    {
        //
        if (!Schema::hasTable("requests_returns")){
            Schema::create('requests_returns',function (Blueprint $table){
                $table->increments('id');
                $table->integer('request_id')->unsigned()->nullable();
                $table->integer('client_id')->unsigned()->nullable();
                $table->foreign('request_id')->references('id')->on('requetes');
                $table->foreign('client_id')->references('id')->on('clients');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
