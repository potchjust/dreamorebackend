<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToPurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchases', function (Blueprint $table) {
            $table->integer('state');
            $table->dropForeign('purchases_requete_id_foreign');
            $table->dropColumn('requete_id');
            /*drafts_id*/
            $table->integer('draft_id')->unsigned()->nullable();
            $table->foreign('draft_id')->references('id')->on('drafts');
            $table->dropColumn('pay_before');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchases', function (Blueprint $table) {
            //
        });
    }
}
