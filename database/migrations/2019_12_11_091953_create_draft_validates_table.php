<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDraftValidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('draft_validates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("draft_id")->unsigned()->nullable();
            $table->foreign("draft_id")->references('id')->on('drafts');
            $table->string("bill_url");
            $table->integer("approved_by")->nullable()->unsigned();
            $table->foreign("approved_by")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('draft_validates');
    }
}
