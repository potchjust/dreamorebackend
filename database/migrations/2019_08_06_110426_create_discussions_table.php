<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscussionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('discussions')){
            Schema::create('discussions', function (Blueprint $table) {
                $table->increments('id');
                $table->string('content');
                $table->dateTime('discuss_at');
                $table->integer('request_id')->unsigned()->nullable();
                $table->integer('client_id')->unsigned()->nullable();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->foreign('request_id')->references('id')->on('requetes');
                $table->foreign('admin_id')->references('id')->on('admins');
                $table->foreign('client_id')->references('id')->on('clients');
                $table->string('billUrl')->nullable();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discussions',function (Blueprint $blueprint){
            $blueprint->dropForeign('discussions_request_id_foreign');
            $blueprint->dropForeign('discussions_client_id_foreign');
            $blueprint->dropColumn('request_id');
            $blueprint->dropColumn('client_id');

        });
        Schema::dropIfExists('discussions');
    }
}
