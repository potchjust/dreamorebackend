<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       /* Schema::table('requetes', function (Blueprint $table) {
            $table->string('comments')->nullable()->default('No comments');
        });*/

       if (!Schema::hasTable("clients")){
           Schema::table('clients',function (Blueprint $table){
               $table->string('country_code')->nullable();
           });
       }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('table', function (Blueprint $table) {
            //
        });
    }
}
