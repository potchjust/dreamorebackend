<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('clients')){
            Schema::create('clients', function (Blueprint $table) {
                $table->increments('id');
                $table->string('lname')->nullable();
                $table->string('fname')->nullable();
                $table->string('address')->nullable();
                $table->string('email')->nullable();
                $table->string('telephone');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $admin_table) {
            $admin_table->dropForeign('clients_user_id_foreign');
            $admin_table->dropColumn('user_id');
        });
        Schema::dropIfExists('clients');
    }
}
