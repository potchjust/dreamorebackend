<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequetesTable extends Migration
{

    public function up()
    {

        if (!Schema::hasTable("requetes")){
            Schema::create('requetes', function (Blueprint $table) {
                $table->increments('id');
                $table->dateTime('request_date');
                $table->integer('statut');

                $table->integer('from_id')->unsigned()->nullable();//celui qui envoit la reqûete
                $table->integer('to_id')->unsigned()->nullable();//celui qui reçoit la reqûete
                $table->foreign('from_id', 'from_admin_id')->references('id')->on('admins');
                $table->foreign('to_id', 'to_admin_id')->references('id')->on('admins');
                $table->integer('draft_id')->unsigned()->nullable();
                $table->foreign('draft_id')->references('id')->on('drafts');

            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requetes', function (Blueprint $blueprint) {
            $blueprint->dropForeign('requetes_draft_id_foreign');
            $blueprint->dropColumn('draft_id');
            $blueprint->dropForeign('from_admin_id_foreign_key');
            $blueprint->dropForeign('to_admin_id_foreign_key');
            $blueprint->dropColumn('from_admin_id');
            $blueprint->dropColumn('to_admin_id');
        });
        Schema::dropIfExists('requetes');
    }
}
