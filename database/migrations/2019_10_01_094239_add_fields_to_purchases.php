<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToPurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn("purchases",'rest_to_pay')&&!Schema::hasColumn("purchases",'mountant_paid')&&!Schema::hasColumn("purchases",'pay_before')){
            Schema::table('purchases', function (Blueprint $table) {
                $table->integer('rest_to_pay')->nullable();
                $table->integer('mountant_paid')->nullable();
                $table->date('pay_before')->nullable();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchases', function (Blueprint $table) {
            //
        });
    }
}
