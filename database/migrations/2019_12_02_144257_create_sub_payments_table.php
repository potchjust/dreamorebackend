<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('sub_payments')){
            Schema::create('sub_payments', function (Blueprint $table) {
                $table->increments('id');
                $table->integer("paid");
                $table->integer("payment_id")->unsigned()->nullable();
                $table->foreign("payment_id")->references('id')->on('payments');
                $table->dateTime("paid_at");
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_payments');
    }
}
