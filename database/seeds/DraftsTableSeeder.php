<?php

use App\Drafts;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class DraftsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Drafts::create([
            'draft_subject' => 'Inquiry',
            'draft_quantity' => 15,
            'draft_content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est in reprehenderit sunt ullam. 
                Asperiores impedit in laboriosam quidem. Accusamus animi, 
            aspernatur aut autem consequuntur cupiditate inventore minus molestiae obcaecati saepe.',
            'draft_deliver_mode' => 1,
            'client_id' => 1,
            'received_at' => \Carbon\Carbon::now(),
            'process_at'=>\Carbon\Carbon::now()
        ]);
    }
}
