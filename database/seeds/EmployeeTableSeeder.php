<?php

use Illuminate\Database\Seeder;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $admin=\App\Admin::create([
            'username'=>'admin@dreammore.co',
            'password'=>\Illuminate\Support\Facades\Hash::make('admin@dreammore.co'),
            'role_id'=>1
        ]);
        $justin=\App\Admin::create([
            'username'=>'alfred@dreammore.co',
            'password'=>\Illuminate\Support\Facades\Hash::make('justin@dreammore.co'),
            'role_id'=>1
        ]);
        $user = $admin->user()->create();
        $user2 = $justin->user()->create();


    }
}
