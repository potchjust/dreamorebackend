<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $skiel = \App\Admin::create([
            'username' => 'philipine@dreammore.co',
            'password' => Hash::make('philipine@dreammore.co'),
            'role_id' => 2
        ]);

        $user2 = $skiel->user()->create();
    }
}

