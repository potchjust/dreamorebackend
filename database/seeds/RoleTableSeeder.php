<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Role::create([
            'role_name' => 'administrator'
        ]);
        \App\Role::create([
            'role_name' => 'request_manager'
        ]);
        \App\Role::create([
            'role_name' => 'accountant'
        ]);
    }
}
