<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Client::create(
            [
                'lname'     => 'desk',
                'fname'     => 'help',
                'address'   => 'Lomé-Togo',
                'email'     => 'heldesk@dreamore.com',
                'telephone' => '228 90 34 84 49',          
            ]
        );
    }
}
