
update:
	composer update & npm update
run:
	php artisan serve & yarn run hot & laravel-echo-server start

runnet:
	php artisan serve --host=192.168.1.68 & yarn run hot
clear:
	php artisan cache:clear & php artisan config:clear

