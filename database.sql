-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le :  lun. 29 juin 2020 à 15:24
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `dreamore_dreamorebackend`
--

-- --------------------------------------------------------

--
-- Structure de la table `admins`
--

CREATE TABLE `admins` (
                          `id` int(10) UNSIGNED NOT NULL,
                          `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                          `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                          `role_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `role_id`) VALUES
(1, 'admin@dreammore.co', '$2y$10$im3N4XTcHoV73Bk1CQDe8Oq7jGdZqWWLtm1L8EBYg/yypoBwJqAIa', 1),
(2, 'justin@dreammore.co', '$2y$10$Js5r0Q9d7WbeKnfjMLiot.8z6uqJuDniLqLVeMqfG2CAIvOymPwa6', 1),
(6, 'philipine@dreammore.co', '$2y$10$VWjd38ruNGJB8skiBk.no.q7TrCp4gYbf.AEteNUJUuPSuF9X6.2i', 2);

-- --------------------------------------------------------

--
-- Structure de la table `attachments`
--

CREATE TABLE `attachments` (
                               `id` int(10) UNSIGNED NOT NULL,
                               `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `file_extension` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `attachable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `attachable_id` int(10) UNSIGNED DEFAULT NULL,
                               `attachment_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `isBill` int(11) DEFAULT '0',
                               `validation` int(11) DEFAULT NULL,
                               `is_attached` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `attachments`
--

INSERT INTO `attachments` (`id`, `name`, `file_extension`, `attachable_type`, `attachable_id`, `attachment_url`, `isBill`, `validation`, `is_attached`) VALUES
(1, 'GESTION_FACILE_04Jan2019.pdf', 'pdf', 'App\\Drafts', 2, 'http://192.168.1.68:8000/storage/uploads/apps/vraWiHyOvIOPQ4j3QpyeEP4jaCdVUlEBk3PUscP8.pdf', 0, 1, 0),
(2, 'GESTION FACILE 04Jan2019.pdf', 'pdf', 'App\\Drafts', 2, 'http://192.168.1.68:8000/storage/webApp/documents/wf5FIVN2lKpQToKdPKD7LQdQKF7KMkfIp48XqIWe.pdf', 0, 3, 0);

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

CREATE TABLE `clients` (
                           `id` int(10) UNSIGNED NOT NULL,
                           `lname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                           `fname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                           `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                           `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                           `telephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                           `fcm_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                           `country_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `clients`
--

INSERT INTO `clients` (`id`, `lname`, `fname`, `address`, `email`, `telephone`, `fcm_token`, `country_code`) VALUES
(1, 'Justin', 'POTCHONA', NULL, NULL, '22890328783', 'd6v1gSgRWBM:APA91bGVtP4YiTkuNiZbaxba5kHNKVJNVa9kkqs0t-Seth82R0MaGfkGTUCTWwvn3uujf0zQGIZRCMJsWrJGn_qgRlfiYmpVfj2z7cJgmWa244vjOjcc8Lj-GTShNAmOWbSHoxEuvRRk', '234234234');

-- --------------------------------------------------------

--
-- Structure de la table `discussions`
--

CREATE TABLE `discussions` (
                               `id` int(10) UNSIGNED NOT NULL,
                               `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
                               `discuss_at` datetime NOT NULL,
                               `draft_id` int(10) UNSIGNED DEFAULT NULL,
                               `from_id` int(10) UNSIGNED DEFAULT NULL,
                               `to_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `discussions`
--

INSERT INTO `discussions` (`id`, `content`, `discuss_at`, `draft_id`, `from_id`, `to_id`) VALUES
(3, 'asdasd', '2020-06-23 10:14:14', 1, 11, 33),
(4, 'dsd', '2020-06-23 10:17:48', 1, 33, 11),
(5, 'lorem ipsum', '2020-06-23 14:22:09', 22, 33, 11);

-- --------------------------------------------------------

--
-- Structure de la table `drafts`
--

CREATE TABLE `drafts` (
                          `id` int(10) UNSIGNED NOT NULL,
                          `draft_subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                          `draft_quantity` int(11) NOT NULL,
                          `draft_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
                          `draft_deliver_mode` int(11) NOT NULL,
                          `client_id` int(10) UNSIGNED DEFAULT NULL,
                          `admin_id` int(10) UNSIGNED DEFAULT NULL,
                          `received_at` datetime NOT NULL,
                          `process_at` datetime NOT NULL,
                          `state` int(11) DEFAULT NULL,
                          `to_validate` int(10) UNSIGNED DEFAULT NULL,
                          `total` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `drafts`
--

INSERT INTO `drafts` (`id`, `draft_subject`, `draft_quantity`, `draft_content`, `draft_deliver_mode`, `client_id`, `admin_id`, `received_at`, `process_at`, `state`, `to_validate`, `total`) VALUES
(1, 'GH', 0, 'Ghuyy', 1, 1, NULL, '2020-06-19 14:35:40', '1970-01-01 00:00:00', 0, NULL, NULL),
(22, '324', 123, 'Qweqwe', 2, 1, NULL, '2020-06-21 19:51:52', '1970-01-01 00:00:00', 0, NULL, NULL),
(23, 'Wee', 0, 'Qweqwe', 2, 1, NULL, '2020-06-21 19:53:24', '1970-01-01 00:00:00', 0, NULL, NULL),
(24, 'Skies', 0, 'Sadasd', 2, 1, NULL, '2020-06-21 19:54:43', '1970-01-01 00:00:00', 0, NULL, NULL),
(25, 'Roger', 0, 'ASasaS', 2, 1, NULL, '2020-06-21 19:55:51', '1970-01-01 00:00:00', 0, NULL, NULL),
(26, 'Qwe', 0, 'Asd', 2, 1, NULL, '2020-06-21 20:12:56', '1970-01-01 00:00:00', 0, NULL, NULL),
(27, 'Das', 0, 'Weqwe', 2, 1, NULL, '2020-06-21 20:14:16', '1970-01-01 00:00:00', 0, NULL, NULL),
(28, '12', 123, 'Sdsd', 2, 1, NULL, '2020-06-21 20:38:48', '1970-01-01 00:00:00', 0, NULL, NULL),
(29, 'Daw', 0, 'Asdasd', 2, 1, NULL, '2020-06-21 20:40:02', '1970-01-01 00:00:00', 0, NULL, NULL),
(30, 'asd', 123, '1234', 1, 1, NULL, '2020-06-24 16:03:41', '1970-01-01 00:00:00', 0, NULL, NULL),
(31, 'asd', 123, '1234', 1, 1, NULL, '2020-06-24 16:04:20', '1970-01-01 00:00:00', 0, NULL, NULL),
(32, 'asd', 123, '1234', 1, 1, NULL, '2020-06-24 16:04:38', '1970-01-01 00:00:00', 0, NULL, NULL),
(33, 'asd', 123, '1234', 1, 1, NULL, '2020-06-24 16:05:07', '1970-01-01 00:00:00', 0, NULL, NULL),
(34, 'asd', 123, '1234', 1, 1, NULL, '2020-06-24 16:05:48', '1970-01-01 00:00:00', 0, NULL, NULL),
(35, 'asd', 123, '1234', 1, 1, NULL, '2020-06-24 16:06:33', '1970-01-01 00:00:00', 0, NULL, NULL),
(36, 'asd', 123, '1234', 1, 1, NULL, '2020-06-24 16:06:34', '1970-01-01 00:00:00', 0, NULL, NULL),
(37, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:04:44', '1970-01-01 00:00:00', 0, NULL, NULL),
(38, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:05:35', '1970-01-01 00:00:00', 0, NULL, NULL),
(39, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:06:48', '1970-01-01 00:00:00', 0, NULL, NULL),
(40, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:07:14', '1970-01-01 00:00:00', 0, NULL, NULL),
(41, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:08:13', '1970-01-01 00:00:00', 0, NULL, NULL),
(42, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:08:58', '1970-01-01 00:00:00', 0, NULL, NULL),
(43, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:10:28', '1970-01-01 00:00:00', 0, NULL, NULL),
(44, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:12:03', '1970-01-01 00:00:00', 0, NULL, NULL),
(45, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:16:15', '1970-01-01 00:00:00', 0, NULL, NULL),
(46, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:18:34', '1970-01-01 00:00:00', 0, NULL, NULL),
(47, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:18:52', '1970-01-01 00:00:00', 0, NULL, NULL),
(48, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:20:54', '1970-01-01 00:00:00', 0, NULL, NULL),
(49, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:30:52', '1970-01-01 00:00:00', 0, NULL, NULL),
(50, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:36:01', '1970-01-01 00:00:00', 0, NULL, NULL),
(51, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:36:54', '1970-01-01 00:00:00', 0, NULL, NULL),
(52, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:38:15', '1970-01-01 00:00:00', 0, NULL, NULL),
(53, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:39:00', '1970-01-01 00:00:00', 0, NULL, NULL),
(54, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:44:32', '1970-01-01 00:00:00', 0, NULL, NULL),
(55, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:45:44', '1970-01-01 00:00:00', 0, NULL, NULL),
(56, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:45:44', '1970-01-01 00:00:00', 0, NULL, NULL),
(57, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:46:12', '1970-01-01 00:00:00', 0, NULL, NULL),
(58, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:46:14', '1970-01-01 00:00:00', 0, NULL, NULL),
(59, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:49:46', '1970-01-01 00:00:00', 0, NULL, NULL),
(60, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:50:00', '1970-01-01 00:00:00', 0, NULL, NULL),
(61, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:51:51', '1970-01-01 00:00:00', 0, NULL, NULL),
(62, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:52:18', '1970-01-01 00:00:00', 0, NULL, NULL),
(63, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:53:12', '1970-01-01 00:00:00', 0, NULL, NULL),
(64, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:53:57', '1970-01-01 00:00:00', 0, NULL, NULL),
(65, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:54:45', '1970-01-01 00:00:00', 0, NULL, NULL),
(66, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:55:21', '1970-01-01 00:00:00', 0, NULL, NULL),
(67, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 10:56:40', '1970-01-01 00:00:00', 0, NULL, NULL),
(68, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 11:19:53', '1970-01-01 00:00:00', 0, NULL, NULL),
(69, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 11:20:10', '1970-01-01 00:00:00', 0, NULL, NULL),
(70, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 11:20:41', '1970-01-01 00:00:00', 0, NULL, NULL),
(71, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 11:20:44', '1970-01-01 00:00:00', 0, NULL, NULL),
(72, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 11:21:36', '1970-01-01 00:00:00', 0, NULL, NULL),
(73, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 11:25:12', '1970-01-01 00:00:00', 0, NULL, NULL),
(74, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 11:26:09', '1970-01-01 00:00:00', 0, NULL, NULL),
(75, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 11:26:37', '1970-01-01 00:00:00', 0, NULL, NULL),
(76, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 11:26:48', '1970-01-01 00:00:00', 0, NULL, NULL),
(77, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 11:27:46', '1970-01-01 00:00:00', 0, NULL, NULL),
(78, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 11:28:19', '1970-01-01 00:00:00', 0, NULL, NULL),
(79, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 11:29:00', '1970-01-01 00:00:00', 0, NULL, NULL),
(80, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 11:29:58', '1970-01-01 00:00:00', 0, NULL, NULL),
(81, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 11:30:25', '1970-01-01 00:00:00', 0, NULL, NULL),
(82, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 11:30:45', '1970-01-01 00:00:00', 0, NULL, NULL),
(83, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 11:31:40', '1970-01-01 00:00:00', 0, NULL, NULL),
(84, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 14:32:00', '1970-01-01 00:00:00', 0, NULL, NULL),
(85, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 14:32:22', '1970-01-01 00:00:00', 0, NULL, NULL),
(86, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 14:50:12', '1970-01-01 00:00:00', 0, NULL, NULL),
(87, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 14:51:28', '1970-01-01 00:00:00', 0, NULL, NULL),
(88, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 14:56:39', '1970-01-01 00:00:00', 0, NULL, NULL),
(89, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 14:57:03', '1970-01-01 00:00:00', 0, NULL, NULL),
(90, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 14:58:37', '1970-01-01 00:00:00', 0, NULL, NULL),
(91, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 14:59:11', '1970-01-01 00:00:00', 0, NULL, NULL),
(92, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 14:59:58', '1970-01-01 00:00:00', 0, NULL, NULL),
(93, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 15:00:04', '1970-01-01 00:00:00', 0, NULL, NULL),
(94, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 15:00:18', '1970-01-01 00:00:00', 0, NULL, NULL),
(95, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 15:00:26', '1970-01-01 00:00:00', 0, NULL, NULL),
(96, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 15:00:35', '1970-01-01 00:00:00', 0, NULL, NULL),
(97, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 15:00:39', '1970-01-01 00:00:00', 0, NULL, NULL),
(98, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 15:01:25', '1970-01-01 00:00:00', 0, NULL, NULL),
(99, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 15:05:04', '1970-01-01 00:00:00', 0, NULL, NULL),
(100, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 15:05:22', '1970-01-01 00:00:00', 0, NULL, NULL),
(101, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 15:05:31', '1970-01-01 00:00:00', 0, NULL, NULL),
(102, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 15:05:42', '1970-01-01 00:00:00', 0, NULL, NULL),
(103, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 15:11:33', '1970-01-01 00:00:00', 0, NULL, NULL),
(104, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 15:11:51', '1970-01-01 00:00:00', 0, NULL, NULL),
(105, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 15:12:11', '1970-01-01 00:00:00', 0, NULL, NULL),
(106, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 15:13:01', '1970-01-01 00:00:00', 0, NULL, NULL),
(107, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 15:50:03', '1970-01-01 00:00:00', 0, NULL, NULL),
(108, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 15:50:32', '1970-01-01 00:00:00', 0, NULL, NULL),
(109, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 16:01:03', '1970-01-01 00:00:00', 0, NULL, NULL),
(110, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 16:01:24', '1970-01-01 00:00:00', 0, NULL, NULL),
(111, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 16:02:05', '1970-01-01 00:00:00', 0, NULL, NULL),
(112, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 16:07:48', '1970-01-01 00:00:00', 0, NULL, NULL),
(113, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 16:08:32', '1970-01-01 00:00:00', 0, NULL, NULL),
(114, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 16:10:18', '1970-01-01 00:00:00', 0, NULL, NULL),
(115, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 16:10:26', '1970-01-01 00:00:00', 0, NULL, NULL),
(116, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 16:11:28', '1970-01-01 00:00:00', 0, NULL, NULL),
(117, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 16:12:43', '1970-01-01 00:00:00', 0, NULL, NULL),
(118, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 16:12:51', '1970-01-01 00:00:00', 0, NULL, NULL),
(119, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 16:15:59', '1970-01-01 00:00:00', 0, NULL, NULL),
(120, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 16:16:47', '1970-01-01 00:00:00', 0, NULL, NULL),
(121, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 16:17:01', '1970-01-01 00:00:00', 0, NULL, NULL),
(122, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 16:18:51', '1970-01-01 00:00:00', 0, NULL, NULL),
(123, 'asd', 123, '1234', 1, 1, NULL, '2020-06-25 16:19:15', '1970-01-01 00:00:00', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `draft_cancels`
--

CREATE TABLE `draft_cancels` (
                                 `id` int(10) UNSIGNED NOT NULL,
                                 `draft_id` int(10) UNSIGNED DEFAULT NULL,
                                 `bill_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `cancel_by` int(10) UNSIGNED DEFAULT NULL,
                                 `reason` longtext COLLATE utf8mb4_unicode_ci,
                                 `is_admin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `draft_validates`
--

CREATE TABLE `draft_validates` (
                                   `id` int(10) UNSIGNED NOT NULL,
                                   `draft_id` int(10) UNSIGNED DEFAULT NULL,
                                   `bill_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `approved_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `draft_validates`
--

INSERT INTO `draft_validates` (`id`, `draft_id`, `bill_url`, `approved_by`) VALUES
(1, 2, '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `fournisseurs`
--

CREATE TABLE `fournisseurs` (
                                `id` int(11) NOT NULL,
                                `denomination` longtext,
                                `contact` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
                              `id` int(10) UNSIGNED NOT NULL,
                              `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
                                       `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `user_id` int(11) DEFAULT NULL,
                                       `client_id` int(10) UNSIGNED NOT NULL,
                                       `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                       `scopes` text COLLATE utf8mb4_unicode_ci,
                                       `revoked` tinyint(1) NOT NULL,
                                       `created_at` timestamp NULL DEFAULT NULL,
                                       `updated_at` timestamp NULL DEFAULT NULL,
                                       `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
                                    `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                    `user_id` int(11) NOT NULL,
                                    `client_id` int(10) UNSIGNED NOT NULL,
                                    `scopes` text COLLATE utf8mb4_unicode_ci,
                                    `revoked` tinyint(1) NOT NULL,
                                    `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
                                 `id` int(10) UNSIGNED NOT NULL,
                                 `user_id` int(11) DEFAULT NULL,
                                 `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `personal_access_client` tinyint(1) NOT NULL,
                                 `password_client` tinyint(1) NOT NULL,
                                 `revoked` tinyint(1) NOT NULL,
                                 `created_at` timestamp NULL DEFAULT NULL,
                                 `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
                                                 `id` int(10) UNSIGNED NOT NULL,
                                                 `client_id` int(10) UNSIGNED NOT NULL,
                                                 `created_at` timestamp NULL DEFAULT NULL,
                                                 `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
                                        `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `revoked` tinyint(1) NOT NULL,
                                        `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

CREATE TABLE `orders` (
                          `id` int(10) UNSIGNED NOT NULL,
                          `purchase_id` int(10) UNSIGNED DEFAULT NULL,
                          `ship_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
                                   `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `purchases`
--

CREATE TABLE `purchases` (
                             `id` int(10) UNSIGNED NOT NULL,
                             `total_price` int(11) NOT NULL,
                             `purchase_state` int(11) NOT NULL DEFAULT '0',
                             `draft_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `requests_returns`
--

CREATE TABLE `requests_returns` (
                                    `id` int(10) UNSIGNED NOT NULL,
                                    `request_id` int(10) UNSIGNED DEFAULT NULL,
                                    `client_id` int(10) UNSIGNED DEFAULT NULL,
                                    `attachment_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `requetes`
--

CREATE TABLE `requetes` (
                            `id` int(10) UNSIGNED NOT NULL,
                            `request_date` datetime NOT NULL,
                            `statut` int(11) NOT NULL,
                            `from_id` int(10) UNSIGNED DEFAULT NULL,
                            `to_id` int(10) UNSIGNED DEFAULT NULL,
                            `draft_id` int(10) UNSIGNED DEFAULT NULL,
                            `comment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'No comments',
                            `cancel_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `requetes`
--

INSERT INTO `requetes` (`id`, `request_date`, `statut`, `from_id`, `to_id`, `draft_id`, `comment`, `cancel_by`) VALUES
(1, '2020-06-19 11:28:25', -5, NULL, NULL, 1, 'No comments', NULL),
(2, '2020-06-19 11:32:40', -5, NULL, NULL, 2, 'No comments', NULL),
(3, '2020-06-19 11:34:04', -5, NULL, NULL, 3, 'No comments', NULL),
(4, '2020-06-19 11:48:06', -5, NULL, NULL, 4, 'No comments', NULL),
(5, '2020-06-19 12:27:47', -5, NULL, NULL, 5, 'No comments', NULL),
(6, '2020-06-19 12:29:47', -5, NULL, NULL, 6, 'No comments', NULL),
(7, '2020-06-19 12:34:27', -5, NULL, NULL, 7, 'No comments', NULL),
(8, '2020-06-19 12:36:25', -5, NULL, NULL, 8, 'No comments', NULL),
(9, '2020-06-19 12:44:05', -5, NULL, NULL, 9, 'No comments', NULL),
(10, '2020-06-19 13:06:51', -5, NULL, NULL, 10, 'No comments', NULL),
(11, '2020-06-19 14:35:40', -5, NULL, NULL, 1, 'No comments', NULL),
(12, '2020-06-19 14:40:09', -5, NULL, NULL, 2, 'No comments', NULL),
(13, '2020-06-21 13:50:00', -5, NULL, NULL, 3, 'No comments', NULL),
(14, '2020-06-21 14:12:56', -5, NULL, NULL, 4, 'No comments', NULL),
(15, '2020-06-21 14:13:52', -5, NULL, NULL, 5, 'No comments', NULL),
(16, '2020-06-21 14:14:50', -5, NULL, NULL, 6, 'No comments', NULL),
(17, '2020-06-21 14:17:04', -5, NULL, NULL, 7, 'No comments', NULL),
(18, '2020-06-21 14:18:48', -5, NULL, NULL, 8, 'No comments', NULL),
(19, '2020-06-21 14:19:27', -5, NULL, NULL, 9, 'No comments', NULL),
(20, '2020-06-21 14:25:58', -5, NULL, NULL, 10, 'No comments', NULL),
(21, '2020-06-21 14:29:00', -5, NULL, NULL, 11, 'No comments', NULL),
(22, '2020-06-21 19:22:10', -5, NULL, NULL, 12, 'No comments', NULL),
(23, '2020-06-21 19:23:31', -5, NULL, NULL, 13, 'No comments', NULL),
(24, '2020-06-21 19:31:14', -5, NULL, NULL, 14, 'No comments', NULL),
(25, '2020-06-21 19:32:31', -5, NULL, NULL, 15, 'No comments', NULL),
(26, '2020-06-21 19:35:07', -5, NULL, NULL, 16, 'No comments', NULL),
(27, '2020-06-21 19:36:20', -5, NULL, NULL, 17, 'No comments', NULL),
(28, '2020-06-21 19:46:59', -5, NULL, NULL, 18, 'No comments', NULL),
(29, '2020-06-21 19:47:52', -5, NULL, NULL, 19, 'No comments', NULL),
(30, '2020-06-21 19:48:43', -5, NULL, NULL, 20, 'No comments', NULL),
(31, '2020-06-21 19:50:20', -5, NULL, NULL, 21, 'No comments', NULL),
(32, '2020-06-21 19:51:52', -5, NULL, NULL, 22, 'No comments', NULL),
(33, '2020-06-21 19:53:24', -5, NULL, NULL, 23, 'No comments', NULL),
(34, '2020-06-21 19:54:43', -5, NULL, NULL, 24, 'No comments', NULL),
(35, '2020-06-21 19:55:51', -5, NULL, NULL, 25, 'No comments', NULL),
(36, '2020-06-21 20:12:56', -5, NULL, NULL, 26, 'No comments', NULL),
(37, '2020-06-21 20:14:16', -5, NULL, NULL, 27, 'No comments', NULL),
(38, '2020-06-21 20:38:48', -5, NULL, NULL, 28, 'No comments', NULL),
(39, '2020-06-21 20:40:02', -5, NULL, NULL, 29, 'No comments', NULL),
(40, '2020-06-24 16:03:41', -5, NULL, NULL, 30, 'No comments', NULL),
(41, '2020-06-24 16:04:20', -5, NULL, NULL, 31, 'No comments', NULL),
(42, '2020-06-24 16:04:38', -5, NULL, NULL, 32, 'No comments', NULL),
(43, '2020-06-24 16:05:48', -5, NULL, NULL, 34, 'No comments', NULL),
(44, '2020-06-24 16:06:33', -5, NULL, NULL, 35, 'No comments', NULL),
(45, '2020-06-24 16:06:34', -5, NULL, NULL, 36, 'No comments', NULL),
(46, '2020-06-25 10:04:44', -5, NULL, NULL, 37, 'No comments', NULL),
(47, '2020-06-25 10:05:35', -5, NULL, NULL, 38, 'No comments', NULL),
(48, '2020-06-25 10:06:48', -5, NULL, NULL, 39, 'No comments', NULL),
(49, '2020-06-25 10:07:14', -5, NULL, NULL, 40, 'No comments', NULL),
(50, '2020-06-25 10:08:13', -5, NULL, NULL, 41, 'No comments', NULL),
(51, '2020-06-25 10:08:58', -5, NULL, NULL, 42, 'No comments', NULL),
(52, '2020-06-25 10:10:28', -5, NULL, NULL, 43, 'No comments', NULL),
(53, '2020-06-25 10:12:03', -5, NULL, NULL, 44, 'No comments', NULL),
(54, '2020-06-25 10:16:15', -5, NULL, NULL, 45, 'No comments', NULL),
(55, '2020-06-25 10:18:34', -5, NULL, NULL, 46, 'No comments', NULL),
(56, '2020-06-25 10:18:52', -5, NULL, NULL, 47, 'No comments', NULL),
(57, '2020-06-25 10:20:54', -5, NULL, NULL, 48, 'No comments', NULL),
(58, '2020-06-25 10:30:52', -5, NULL, NULL, 49, 'No comments', NULL),
(59, '2020-06-25 10:36:01', -5, NULL, NULL, 50, 'No comments', NULL),
(60, '2020-06-25 10:36:54', -5, NULL, NULL, 51, 'No comments', NULL),
(61, '2020-06-25 10:38:15', -5, NULL, NULL, 52, 'No comments', NULL),
(62, '2020-06-25 10:39:00', -5, NULL, NULL, 53, 'No comments', NULL),
(63, '2020-06-25 10:44:32', -5, NULL, NULL, 54, 'No comments', NULL),
(64, '2020-06-25 10:45:44', -5, NULL, NULL, 55, 'No comments', NULL),
(65, '2020-06-25 10:45:44', -5, NULL, NULL, 56, 'No comments', NULL),
(66, '2020-06-25 10:46:12', -5, NULL, NULL, 57, 'No comments', NULL),
(67, '2020-06-25 10:46:14', -5, NULL, NULL, 58, 'No comments', NULL),
(68, '2020-06-25 10:49:46', -5, NULL, NULL, 59, 'No comments', NULL),
(69, '2020-06-25 10:50:00', -5, NULL, NULL, 60, 'No comments', NULL),
(70, '2020-06-25 10:51:51', -5, NULL, NULL, 61, 'No comments', NULL),
(71, '2020-06-25 10:52:18', -5, NULL, NULL, 62, 'No comments', NULL),
(72, '2020-06-25 10:53:12', -5, NULL, NULL, 63, 'No comments', NULL),
(73, '2020-06-25 10:53:57', -5, NULL, NULL, 64, 'No comments', NULL),
(74, '2020-06-25 10:54:45', -5, NULL, NULL, 65, 'No comments', NULL),
(75, '2020-06-25 10:55:21', -5, NULL, NULL, 66, 'No comments', NULL),
(76, '2020-06-25 10:56:40', -5, NULL, NULL, 67, 'No comments', NULL),
(77, '2020-06-25 11:19:53', -5, NULL, NULL, 68, 'No comments', NULL),
(78, '2020-06-25 11:20:10', -5, NULL, NULL, 69, 'No comments', NULL),
(79, '2020-06-25 11:20:41', -5, NULL, NULL, 70, 'No comments', NULL),
(80, '2020-06-25 11:20:44', -5, NULL, NULL, 71, 'No comments', NULL),
(81, '2020-06-25 11:21:36', -5, NULL, NULL, 72, 'No comments', NULL),
(82, '2020-06-25 11:25:12', -5, NULL, NULL, 73, 'No comments', NULL),
(83, '2020-06-25 11:26:37', -5, NULL, NULL, 75, 'No comments', NULL),
(84, '2020-06-25 11:26:48', -5, NULL, NULL, 76, 'No comments', NULL),
(85, '2020-06-25 11:27:46', -5, NULL, NULL, 77, 'No comments', NULL),
(86, '2020-06-25 11:28:19', -5, NULL, NULL, 78, 'No comments', NULL),
(87, '2020-06-25 11:29:00', -5, NULL, NULL, 79, 'No comments', NULL),
(88, '2020-06-25 11:29:58', -5, NULL, NULL, 80, 'No comments', NULL),
(89, '2020-06-25 11:30:25', -5, NULL, NULL, 81, 'No comments', NULL),
(90, '2020-06-25 11:30:45', -5, NULL, NULL, 82, 'No comments', NULL),
(91, '2020-06-25 11:31:40', -5, NULL, NULL, 83, 'No comments', NULL),
(92, '2020-06-25 14:32:00', -5, NULL, NULL, 84, 'No comments', NULL),
(93, '2020-06-25 14:32:22', -5, NULL, NULL, 85, 'No comments', NULL),
(94, '2020-06-25 14:50:12', -5, NULL, NULL, 86, 'No comments', NULL),
(95, '2020-06-25 14:51:28', -5, NULL, NULL, 87, 'No comments', NULL),
(96, '2020-06-25 14:56:39', -5, NULL, NULL, 88, 'No comments', NULL),
(97, '2020-06-25 14:57:03', -5, NULL, NULL, 89, 'No comments', NULL),
(98, '2020-06-25 14:58:37', -5, NULL, NULL, 90, 'No comments', NULL),
(99, '2020-06-25 14:59:11', -5, NULL, NULL, 91, 'No comments', NULL),
(100, '2020-06-25 14:59:58', -5, NULL, NULL, 92, 'No comments', NULL),
(101, '2020-06-25 15:00:04', -5, NULL, NULL, 93, 'No comments', NULL),
(102, '2020-06-25 15:00:18', -5, NULL, NULL, 94, 'No comments', NULL),
(103, '2020-06-25 15:00:26', -5, NULL, NULL, 95, 'No comments', NULL),
(104, '2020-06-25 15:00:35', -5, NULL, NULL, 96, 'No comments', NULL),
(105, '2020-06-25 15:00:39', -5, NULL, NULL, 97, 'No comments', NULL),
(106, '2020-06-25 15:01:25', -5, NULL, NULL, 98, 'No comments', NULL),
(107, '2020-06-25 15:05:04', -5, NULL, NULL, 99, 'No comments', NULL),
(108, '2020-06-25 15:05:22', -5, NULL, NULL, 100, 'No comments', NULL),
(109, '2020-06-25 15:05:31', -5, NULL, NULL, 101, 'No comments', NULL),
(110, '2020-06-25 15:05:42', -5, NULL, NULL, 102, 'No comments', NULL),
(111, '2020-06-25 15:11:33', -5, NULL, NULL, 103, 'No comments', NULL),
(112, '2020-06-25 15:11:51', -5, NULL, NULL, 104, 'No comments', NULL),
(113, '2020-06-25 15:12:11', -5, NULL, NULL, 105, 'No comments', NULL),
(114, '2020-06-25 15:13:01', -5, NULL, NULL, 106, 'No comments', NULL),
(115, '2020-06-25 15:50:03', -5, NULL, NULL, 107, 'No comments', NULL),
(116, '2020-06-25 15:50:32', -5, NULL, NULL, 108, 'No comments', NULL),
(117, '2020-06-25 16:01:03', -5, NULL, NULL, 109, 'No comments', NULL),
(118, '2020-06-25 16:01:24', -5, NULL, NULL, 110, 'No comments', NULL),
(119, '2020-06-25 16:02:05', -5, NULL, NULL, 111, 'No comments', NULL),
(120, '2020-06-25 16:07:48', -5, NULL, NULL, 112, 'No comments', NULL),
(121, '2020-06-25 16:08:32', -5, NULL, NULL, 113, 'No comments', NULL),
(122, '2020-06-25 16:10:18', -5, NULL, NULL, 114, 'No comments', NULL),
(123, '2020-06-25 16:10:26', -5, NULL, NULL, 115, 'No comments', NULL),
(124, '2020-06-25 16:11:28', -5, NULL, NULL, 116, 'No comments', NULL),
(125, '2020-06-25 16:12:43', -5, NULL, NULL, 117, 'No comments', NULL),
(126, '2020-06-25 16:12:51', -5, NULL, NULL, 118, 'No comments', NULL),
(127, '2020-06-25 16:15:59', -5, NULL, NULL, 119, 'No comments', NULL),
(128, '2020-06-25 16:16:47', -5, NULL, NULL, 120, 'No comments', NULL),
(129, '2020-06-25 16:17:01', -5, NULL, NULL, 121, 'No comments', NULL),
(130, '2020-06-25 16:18:51', -5, NULL, NULL, 122, 'No comments', NULL),
(131, '2020-06-25 16:19:15', -5, NULL, NULL, 123, 'No comments', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
                         `id` int(10) UNSIGNED NOT NULL,
                         `role_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `role_name`) VALUES
(1, 'administrator'),
(2, 'request_manager'),
(3, 'accountant');

-- --------------------------------------------------------

--
-- Structure de la table `slides`
--

CREATE TABLE `slides` (
                          `id` int(11) NOT NULL,
                          `image_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sub_payments`
--

CREATE TABLE `sub_payments` (
                                `id` int(10) UNSIGNED NOT NULL,
                                `paid` int(11) NOT NULL,
                                `purchase_id` int(10) UNSIGNED DEFAULT NULL,
                                `paid_at` datetime NOT NULL,
                                `rest` int(11) DEFAULT NULL,
                                `identifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                `payment_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
                         `id` int(10) UNSIGNED NOT NULL,
                         `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `user_type`, `user_id`) VALUES
(1, 'App\\Admin', 1),
(2, 'App\\Admin', 2),
(9, 'App\\Admin', 4),
(10, 'App\\Admin', 5),
(11, 'App\\Admin', 6),
(33, 'App\\Client', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `admins`
--
ALTER TABLE `admins`
    ADD PRIMARY KEY (`id`),
    ADD KEY `admins_role_id_foreign` (`role_id`);

--
-- Index pour la table `attachments`
--
ALTER TABLE `attachments`
    ADD PRIMARY KEY (`id`);

--
-- Index pour la table `clients`
--
ALTER TABLE `clients`
    ADD PRIMARY KEY (`id`);

--
-- Index pour la table `discussions`
--
ALTER TABLE `discussions`
    ADD PRIMARY KEY (`id`),
    ADD KEY `discussions_request_id_foreign` (`draft_id`),
    ADD KEY `discussions_admin_id_foreign` (`to_id`),
    ADD KEY `discussions_client_id_foreign` (`from_id`);

--
-- Index pour la table `drafts`
--
ALTER TABLE `drafts`
    ADD PRIMARY KEY (`id`),
    ADD KEY `drafts_client_id_foreign` (`client_id`),
    ADD KEY `drafts_admin_id_foreign` (`admin_id`),
    ADD KEY `drafts_to_validate_foreign` (`to_validate`);

--
-- Index pour la table `draft_cancels`
--
ALTER TABLE `draft_cancels`
    ADD PRIMARY KEY (`id`),
    ADD KEY `draft_cancels_draft_id_foreign` (`draft_id`),
    ADD KEY `draft_cancels_cancel_by_foreign` (`cancel_by`);

--
-- Index pour la table `draft_validates`
--
ALTER TABLE `draft_validates`
    ADD PRIMARY KEY (`id`),
    ADD KEY `draft_validates_draft_id_foreign` (`draft_id`),
    ADD KEY `draft_validates_approved_by_foreign` (`approved_by`);

--
-- Index pour la table `fournisseurs`
--
ALTER TABLE `fournisseurs`
    ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
    ADD PRIMARY KEY (`id`);

--
-- Index pour la table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
    ADD PRIMARY KEY (`id`),
    ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Index pour la table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
    ADD PRIMARY KEY (`id`);

--
-- Index pour la table `oauth_clients`
--
ALTER TABLE `oauth_clients`
    ADD PRIMARY KEY (`id`),
    ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Index pour la table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
    ADD PRIMARY KEY (`id`),
    ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Index pour la table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
    ADD PRIMARY KEY (`id`),
    ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Index pour la table `orders`
--
ALTER TABLE `orders`
    ADD PRIMARY KEY (`id`),
    ADD KEY `orders_purchase_id_foreign` (`purchase_id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
    ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `purchases`
--
ALTER TABLE `purchases`
    ADD PRIMARY KEY (`id`),
    ADD KEY `purchases_draft_id_foreign` (`draft_id`);

--
-- Index pour la table `requests_returns`
--
ALTER TABLE `requests_returns`
    ADD PRIMARY KEY (`id`),
    ADD KEY `requests_returns_request_id_foreign` (`request_id`),
    ADD KEY `requests_returns_client_id_foreign` (`client_id`);

--
-- Index pour la table `requetes`
--
ALTER TABLE `requetes`
    ADD PRIMARY KEY (`id`),
    ADD KEY `from_admin_id` (`from_id`),
    ADD KEY `to_admin_id` (`to_id`),
    ADD KEY `requetes_draft_id_foreign` (`draft_id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
    ADD PRIMARY KEY (`id`);

--
-- Index pour la table `slides`
--
ALTER TABLE `slides`
    ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sub_payments`
--
ALTER TABLE `sub_payments`
    ADD PRIMARY KEY (`id`),
    ADD KEY `sub_purchases_purchase_id_foreign` (`purchase_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `admins`
--
ALTER TABLE `admins`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `attachments`
--
ALTER TABLE `attachments`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `clients`
--
ALTER TABLE `clients`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `discussions`
--
ALTER TABLE `discussions`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `drafts`
--
ALTER TABLE `drafts`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT pour la table `draft_cancels`
--
ALTER TABLE `draft_cancels`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `draft_validates`
--
ALTER TABLE `draft_validates`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `fournisseurs`
--
ALTER TABLE `fournisseurs`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `oauth_clients`
--
ALTER TABLE `oauth_clients`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `orders`
--
ALTER TABLE `orders`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `purchases`
--
ALTER TABLE `purchases`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `requests_returns`
--
ALTER TABLE `requests_returns`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `requetes`
--
ALTER TABLE `requetes`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `slides`
--
ALTER TABLE `slides`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `sub_payments`
--
ALTER TABLE `sub_payments`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `admins`
--
ALTER TABLE `admins`
    ADD CONSTRAINT `admins_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Contraintes pour la table `discussions`
--
ALTER TABLE `discussions`
    ADD CONSTRAINT `discussions_draft_id_foreign` FOREIGN KEY (`draft_id`) REFERENCES `drafts` (`id`),
    ADD CONSTRAINT `discussions_from_id_foreign` FOREIGN KEY (`from_id`) REFERENCES `users` (`id`),
    ADD CONSTRAINT `discussions_to_id_foreign` FOREIGN KEY (`to_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `drafts`
--
ALTER TABLE `drafts`
    ADD CONSTRAINT `drafts_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`),
    ADD CONSTRAINT `drafts_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
    ADD CONSTRAINT `drafts_to_validate_foreign` FOREIGN KEY (`to_validate`) REFERENCES `admins` (`id`);

--
-- Contraintes pour la table `draft_cancels`
--
ALTER TABLE `draft_cancels`
    ADD CONSTRAINT `draft_cancels_cancel_by_foreign` FOREIGN KEY (`cancel_by`) REFERENCES `users` (`id`),
    ADD CONSTRAINT `draft_cancels_draft_id_foreign` FOREIGN KEY (`draft_id`) REFERENCES `drafts` (`id`);

--
-- Contraintes pour la table `draft_validates`
--
ALTER TABLE `draft_validates`
    ADD CONSTRAINT `draft_validates_approved_by_foreign` FOREIGN KEY (`approved_by`) REFERENCES `users` (`id`),
    ADD CONSTRAINT `draft_validates_draft_id_foreign` FOREIGN KEY (`draft_id`) REFERENCES `drafts` (`id`);

--
-- Contraintes pour la table `orders`
--
ALTER TABLE `orders`
    ADD CONSTRAINT `orders_purchase_id_foreign` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`id`);

--
-- Contraintes pour la table `purchases`
--
ALTER TABLE `purchases`
    ADD CONSTRAINT `purchases_draft_id_foreign` FOREIGN KEY (`draft_id`) REFERENCES `drafts` (`id`);

--
-- Contraintes pour la table `requests_returns`
--
ALTER TABLE `requests_returns`
    ADD CONSTRAINT `requests_returns_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
    ADD CONSTRAINT `requests_returns_request_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `requetes` (`id`);

--
-- Contraintes pour la table `requetes`
--
ALTER TABLE `requetes`
    ADD CONSTRAINT `from_admin_id` FOREIGN KEY (`from_id`) REFERENCES `admins` (`id`),
    ADD CONSTRAINT `requetes_draft_id_foreign` FOREIGN KEY (`draft_id`) REFERENCES `drafts` (`id`),
    ADD CONSTRAINT `to_admin_id` FOREIGN KEY (`to_id`) REFERENCES `admins` (`id`);

--
-- Contraintes pour la table `sub_payments`
--
ALTER TABLE `sub_payments`
    ADD CONSTRAINT `sub_purchases_purchase_id_foreign` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`id`);
