var app = require('express')
var server = require('http').Server(app())
var io = require('socket.io')(server)
var redis = require("redis")
require('dotenv').config()

const APP_PORT = process.env.NODE_APP_PORT
server.listen(APP_PORT,()=>{
    console.log("Server running at "+APP_PORT)
});
io.on('connection',function (socket) {
        console.log("Client connecte")
    var redisClient = redis.createClient();
        redisClient.subscribe('message_receive');
        redisClient.subscribe('message_sent');
        redisClient.subscribe("request_receive");
        redisClient.subscribe("request_send_for_validation");//send to admin for validation
        redisClient.subscribe("request_finish_validation")//quand la requete quitte la validation

        redisClient.on("message",function (channel,message) {
            socket.emit(channel,message)
        })
        redisClient.on("disconnect",function () {
            redisClient.quit()
        })
})
